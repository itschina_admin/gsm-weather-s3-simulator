#ifndef __lvgl_hint_H__
#define __lvgl_hint_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "lvgl.h"
#include "lvgl_Project.h"
#include "SYSTEM.h"


#if 1
#define hint_debug(format, ...) lvgl_project_debug("[提示]- ",format,##__VA_ARGS__);
#else
#define hint_debug(format, ...) ;
#endif


#define hint_Button_Num 3
#define hint_event_anXia 100
#define  hint_zuo_you_bmp_y 135



typedef struct
{


	lv_anim_t lv_anim_jin;
	lv_anim_t lv_anim_chu;

	lv_task_t * lvgl_task;

	lv_obj_t * lvgl_Label_text;
	lv_obj_t * lvgl_btn_tuichu;


}_hint_ChuangKou;

extern _hint_ChuangKou hint_ChuangKou;
typedef struct
{
	char buf[20];

	int shijian;
	int DongHua_Flag;

	lv_point_t point;


}_lvgl_hint_GongNeng_Data;

extern _lvgl_hint_GongNeng_Data lvgl_hint_GongNeng_Data;


extern lv_obj_t *lvgl_hint_main_cont;

void lvgl_hint_create(lv_obj_t * Fu, char *text, uint8_t touming, uint8_t shijian);
void lvgl_hint_close(void);
void lvgl_hint_TaskCb(lv_task_t *t);



#ifdef __cplusplus
}
#endif

#endif


