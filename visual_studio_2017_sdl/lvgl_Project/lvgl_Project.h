#ifndef __lvgl_Project_H__
#define __lvgl_Project_H__


#ifdef __cplusplus
extern "C" {
#endif
#define lvgl_win 1

#include "lvgl.h"
#include "main.h"
#include "string.h"
#include "stdio.h"
#include "font.h"
#include "math.h"
#include "SYSTEM.h"

#include "bmp_tianqi_40x40.h"
#include "bmp_tianqi_80x80.h"
#include "bmp_ShuaXin.h"
#include "lvgl_global.h"
#include "lvgl_shuaxin.h"
#include "lvgl_YangShi.h"
#include "lvgl_clock.h"
#include "lvgl_desktop.h"
#include "lvgl_TianQi.h"
#include "lvgl_LaoHuangLi.h"
#include "lvgl_SheZhi.h"
#include "lvgl_SheZhi_wifi.h"
#include "lvgl_SheZhi_yuyan.h"
#include "lvgl_SheZhi_yuyan_anim.h"
#include "lvgl_RiLi.h"
#include "lvgl_RiLi_anim.h"
#include "lvgl_bilibili.h"
#include "lvgl_bilibili_anim.h"
#include "lvgl_hint.h"
#include "lvgl_hint_anim.h"
#include "lvgl_SheZhi_anim.h"
#include "lvgl_shurufa.h"
#include "lvgl_shurufa_anim.h"
#include "lvgl_chulizhong.h"
#include "lvgl_chulizhong_anim.h"
#include "lvgl_wenjian.h"
#include "lvgl_wenjian_anim.h"
#include "lvgl_shares.h"
#include "lvgl_shares_item.h"
#include "lvgl_shares_graph.h"
#include "lvgl_shares_data.h"

//lv_anim_path_linear线性动画
//lv_anim_path_step最后一步更改
//lv_anim_path_ease_in开头很慢
//lv_anim_path_ease_out最后慢
//lv_anim_path_ease_in_out在开始和结束时也很慢
//lv_anim_path_overshoot超出最终值
//lv_anim_path_bounce从最终值反弹一点（就像撞墙一样）

#if 1
#define lvgl_Project_debug(format,...) \
printf("[lvgl_project]-"); \
printf(format,##__VA_ARGS__);
#else
  #define lvgl_Project_debug(format, args...) ;
#endif

#if 1
#define lvgl_project_debug( tag, format, ... ) \
printf("%s",tag); \
printf(format,##__VA_ARGS__);
#else
#define lvgl_project_debug( tag, format, ... ) ;
#endif

#define lvgl_event_anXia 100 //自定义回调函数中的事件

typedef enum {
	type_btn = 0,
	type_lable,
	type_sw,
}lvgl_obj_type;

extern const char *lvgl_globa_text[][10];

#ifdef __cplusplus
}
#endif
#endif
