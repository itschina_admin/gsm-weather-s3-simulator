#ifndef __LVGL_TianQi_H__
#define __LVGL_TianQi_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "lvgl.h"


#if 1
#define TianQi_debug(format, ...) lvgl_project_debug("[����]- ",format,##__VA_ARGS__);
#else
#define TianQi_debug(format, ...) ;
#endif

typedef struct
{
	int ShuaXin_Sta;
	int DongHua_Sta;
	lv_anim_t lv_anim_JinTian;
	int JinTian_y;
	lv_anim_t lv_anim_MingTian;
	int MingTian_y;
	lv_anim_t lv_anim_HouTian;
	int HouTian_y;

	lv_anim_t lv_anim_FengSu;
	lv_anim_t lv_anim_FengXiang;
	lv_anim_t lv_anim_JiangShui;
	lv_anim_t lv_anim_ShiDu;


	lv_anim_t lv_anim_DangTian;


	lv_anim_t lv_anim_RiQi;
	lv_anim_t lv_anim_ShiJian;
	lv_anim_t lv_anim_XingQi;


}_lvgl_TianQi_GongNeng_Data;

extern _lvgl_TianQi_GongNeng_Data lvgl_TianQi_GongNeng_Data;


void lvgl_TianQi_create(lv_obj_t * Fu);
void lvgl_TianQi_close(void);
void lvgl_TianQi_TaskCb(lv_task_t *t);
void lvgl_TianQi_set_ChengShi(char *str);
void lvgl_TianQi_set_ChengShi(char *str);
void lvgl_TianQi_set_ShiJian(int shi, int fen, char *str);
void lvgl_TianQi_set_RiQi(int nian,int yue,int ri, char *str);
void lvgl_TianQi_set_XingQi(int dat);
void lvgl_TianQi_set_WenDu(int dat);
void lvgl_TianQi_set_DangTianQi(int sta,int num,int wendu,char *str,int min, int max);
void lvgl_TianQi_set_TianQi_JinTian(int sta, int num,char *str,int min,int max);
void lvgl_TianQi_set_TianQi_MingTian(int sta, int num, char *str, int min, int max);
void lvgl_TianQi_set_TianQi_HouTian(int sta, int num, char *str, int min, int max);
void lvgl_TianQi_set_FengSu(char *str);
void lvgl_TianQi_set_FengXiang(char *str);
void lvgl_TianQi_set_JiangShui(char *str);
void lvgl_TianQi_set_ShiDu(char *str);
void lvgl_TianQi_set_RiChuRiLuo(int richu_shi, int richu_fen, int riluo_shi, int riluo_fen,char *richu_str,char *riluo_str);

void lvgl_TianQi_shuaxin_kaishi(void);
void lvgl_TianQi_ShuaXin_jieshu(void);
void lvgl_TianQi_DongHua_Jin(void);
void lvgl_TianQi_DongHua_Chu(void);



#ifdef __cplusplus
}
#endif

#endif
