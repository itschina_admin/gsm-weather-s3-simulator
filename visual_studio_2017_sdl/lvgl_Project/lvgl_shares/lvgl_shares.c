#include "lvgl_shares.h"
#include "lvgl_Project.h"
#include "lvgl_SheZhi.h"




_lvgl_shares_data lvgl_shares_data;


void lvgl_shares_event_cb(struct _lv_obj_t * obj, lv_event_t event)
{
	//if (obj == lvgl_shares_data.main_cont)
	{
		shares_debug("cb:%d\r\n", event);
		switch (event)
		{

		case LV_EVENT_DELETE:

			lvgl_shares_closed();
			break;

		case LV_EVENT_PRESSING:


			break;
		case LV_EVENT_PRESSED:


			break;
		case LV_EVENT_DRAG_END:
			shares_debug("拖动后松手\r\n");
			if (lv_obj_get_y(obj) != 0)
			{

				lv_obj_set_y(obj, 0);
			}

			if (lv_obj_get_x(obj) != 0)
			{
				if (lv_obj_get_x(obj) > 100)
				{
					lvgl_shares_close();
				}
				else
				{
					lv_obj_set_x(obj, 0);
				}
			}
			break;
			break;

		case LV_EVENT_RELEASED:

		case LV_EVENT_LONG_PRESSED:

			break;

		default:
			break;

		}

	}
}

static void lvgl_shares_item_cont_event_cb(struct _lv_obj_t* obj, lv_event_t event)
{
	if (obj == lvgl_shares_data.item_cont)
	{
		shares_debug("shares item cont cb:%d\r\n", event);
		switch (event)
		{

		case LV_EVENT_DELETE:

			break;

		case LV_EVENT_PRESSING:


			break;
		case LV_EVENT_PRESSED:


			break;
		case LV_EVENT_DRAG_END:
			shares_debug("拖动后松手\r\n");
			if (lv_obj_get_y(obj) != 0)
			{
				if (lv_obj_get_y(obj) > 0)
				{
					lv_obj_set_y(obj, 0);
				}
				else
					if (lv_obj_get_y(obj) < 0)
					{
						if (lv_obj_get_height(obj) <= lv_obj_get_height(lvgl_shares_data.main_cont))
						{
							lv_obj_set_y(obj, 0);
						}
						else
						{
							if (lv_obj_get_height(obj) - abs(lv_obj_get_y(obj)) < lv_obj_get_height(lvgl_shares_data.main_cont))
							{

								lv_obj_set_y(obj, lv_obj_get_height(lvgl_shares_data.main_cont) - lv_obj_get_height(obj));
							}
						}
					}
			}

			if (lv_obj_get_x(obj) != 0)
			{

			}
			break;

		case LV_EVENT_RELEASED:

		case LV_EVENT_LONG_PRESSED:

			break;

		default:
			break;

		}

	}
}
static void lvgl_shares_item_event_cb(struct _lv_obj_t* obj, lv_event_t event)
{
	//if (obj == lvgl_shares_data.item_cont)
	{
		//shares_debug("shares item cb:%d\r\n", event);
		switch (event)
		{

		case LV_EVENT_DELETE:

			break;

		case LV_EVENT_PRESSING:


			break;
		case LV_EVENT_PRESSED:
			shares_debug("pressed\r\n");

			break;
		case LV_EVENT_DRAG_END:
			shares_debug("拖动后松手\r\n");
	
			break;

		case LV_EVENT_RELEASED:
			shares_debug("released\r\n");


			break;
		case LV_EVENT_LONG_PRESSED:
			shares_debug("long pressed\r\n");


			break;

		default:
			break;

		}

	}
}
void lvgl_shares_create(lv_obj_t * Fu)
{

	if (lvgl_shares_data.main_cont == NULL)
	{

		//----创建容器----//
		lvgl_shares_data.main_cont = lv_cont_create(lv_scr_act(), NULL);
		lv_obj_set_pos(lvgl_shares_data.main_cont, 0, 0);
		lv_obj_set_size(lvgl_shares_data.main_cont, lv_obj_get_width(Fu), lv_obj_get_height(Fu));

		//lv_obj_set_click(lvgl_shares_data.main_cont, true); //启用 / 禁用可点击
		lv_obj_set_drag(lvgl_shares_data.main_cont, true);//启用/禁用对象可拖动
		lv_obj_set_drag_dir(lvgl_shares_data.main_cont, LV_DRAG_DIR_HOR);//设置拖动方向
		//lv_obj_set_drag_throw(lvgl_shares_data.main_cont, false);//启用/禁用拖动后是否有惯性移动
		//lv_obj_set_drag_parent(lvgl_shares_data.main_cont, true); //启用 / 禁用父对象可拖动
		lv_obj_add_style(lvgl_shares_data.main_cont, LV_OBJ_PART_MAIN, &lvgl_black_bg_style);//设置样式
		lv_obj_set_event_cb(lvgl_shares_data.main_cont, lvgl_shares_event_cb);//设置回调函数

		//----日期栏----//
		lvgl_shares_data.bar_cont = lv_cont_create(lvgl_shares_data.main_cont, lvgl_shares_data.main_cont);
		lv_obj_align(lvgl_shares_data.bar_cont, NULL, LV_ALIGN_IN_TOP_MID, 0, 0);
		lv_obj_set_size(lvgl_shares_data.bar_cont, lv_obj_get_width(Fu), 24);
		lv_obj_set_drag_parent(lvgl_shares_data.bar_cont, true); //启用 / 禁用父对象可拖动
		lv_obj_add_style(lvgl_shares_data.bar_cont, LV_OBJ_PART_MAIN, &lvgl_black_bg_style);//设置样式


		lvgl_shares_data.date_label = lv_label_create(lvgl_shares_data.bar_cont, NULL);
		lv_label_set_long_mode(lvgl_shares_data.date_label, LV_LABEL_LONG_EXPAND);
		lv_label_set_anim_speed(lvgl_shares_data.date_label, 5);
		lv_label_set_text(lvgl_shares_data.date_label, "2022-09-03");
		lv_obj_align(lvgl_shares_data.date_label, NULL, LV_ALIGN_IN_TOP_LEFT, 3, 0);
		lv_obj_add_style(lvgl_shares_data.date_label, LV_OBJ_PART_MAIN, &lvgl_font12_style);

		lvgl_shares_data.time_label = lv_label_create(lvgl_shares_data.bar_cont, NULL);
		lv_label_set_long_mode(lvgl_shares_data.time_label, LV_LABEL_LONG_EXPAND);
		lv_label_set_anim_speed(lvgl_shares_data.time_label, 5);
		lv_label_set_text(lvgl_shares_data.time_label, "19:58");
		lv_obj_align(lvgl_shares_data.time_label, NULL, LV_ALIGN_IN_TOP_RIGHT, 0, 0);
		lv_obj_add_style(lvgl_shares_data.time_label, LV_OBJ_PART_MAIN, &lvgl_font12_style);
		//----日期栏----//

		static lv_point_t valid_pos[] = { {0,0}, {0, 1}, {1,1} };
		
		lvgl_shares_data.item_tileview = lv_tileview_create(lvgl_shares_data.main_cont, NULL);
		lv_tileview_set_valid_positions(lvgl_shares_data.item_tileview, valid_pos, 3);
		lv_tileview_set_edge_flash(lvgl_shares_data.item_tileview, true);
		lv_obj_set_pos(lvgl_shares_data.item_tileview, 0, 24);
		lv_obj_set_size(lvgl_shares_data.item_tileview, lv_obj_get_width(lvgl_shares_data.main_cont), lv_obj_get_height(lvgl_shares_data.main_cont)- 24);
		lv_obj_set_style_local_bg_opa(lvgl_shares_data.item_tileview, LV_TILEVIEW_PART_SCROLLBAR, LV_STATE_DEFAULT, LV_OPA_20);
		//lv_page_set_scrollbar_mode(lvgl_shares_data.item_tileview, LV_SCROLLBAR_MODE_AUTO);


		lvgl_shares_data.item_cont = lv_cont_create(lvgl_shares_data.item_tileview, lvgl_shares_data.main_cont);
		lv_obj_set_drag(lvgl_shares_data.item_cont, true);
		lv_obj_set_drag_parent(lvgl_shares_data.item_cont, false);
		lv_obj_set_drag_dir(lvgl_shares_data.item_cont, LV_DRAG_DIR_VER);
		lv_obj_set_drag_throw(lvgl_shares_data.item_cont, true);
		lv_obj_set_pos(lvgl_shares_data.item_cont, 0, 0);
		lv_obj_set_size(lvgl_shares_data.item_cont, lv_obj_get_width(lvgl_shares_data.item_tileview), lv_obj_get_height(lvgl_shares_data.item_tileview));
		//lv_obj_add_style(lvgl_shares_data.item_cont, LV_OBJ_PART_MAIN, &lvgl_blue_bg_style);
		lv_obj_set_event_cb(lvgl_shares_data.item_cont, lvgl_shares_item_cont_event_cb);

		lv_obj_move_background(lvgl_shares_data.item_cont);
		lv_obj_move_background(lvgl_shares_data.item_tileview);

		uint8_t num = 0;
		for (num;num < 10; num++)
		{
			sprintf(lvgl_shares_data.items[num].str_code, "300750");
			sprintf(lvgl_shares_data.items[num].str_name, "test%04d",num);
			sprintf(lvgl_shares_data.items[num].str_zhangfu,"+1.5%%");
			lvgl_shares_data.items[num].mode_zhangfu = num%2? SHARES_ZHANGFU_ZHANG : SHARES_ZHANGFU_JIANG;
			lvgl_shares_data.items[num].point.x = 12;
			lvgl_shares_data.items[num].point.y = num * 10 + num * SHARES_ITEM_HEIGHT;
			lvgl_shares_data.items[num].event_cb = lvgl_shares_item_event_cb;
			lvgl_shares_item_create(lvgl_shares_data.item_cont, &lvgl_shares_data.items[num]);
		}

		//num++;
		sprintf(lvgl_shares_data.items[num].str_code,  " ");
		sprintf(lvgl_shares_data.items[num].str_name,  "addition");
		sprintf(lvgl_shares_data.items[num].str_zhangfu,  " ");
		lvgl_shares_data.items[num].mode_zhangfu = SHARES_ZHANGFU_ZHANG;
		lvgl_shares_data.items[num].point.x = 12;
		lvgl_shares_data.items[num].point.y = num * 10 + num * 52;
		lvgl_shares_data.items[num].event_cb = lvgl_shares_item_event_cb;
		lvgl_shares_item_create(lvgl_shares_data.item_cont, &lvgl_shares_data.items[num]);

		lvgl_shares_data.item_count = num;

		lv_obj_set_height(lvgl_shares_data.item_cont,
			lv_obj_get_height(lvgl_shares_data.main_cont) + (lvgl_shares_data.item_count-3)*(52+10)
		);

		lvgl_task_create(&lvgl_shares_data.task,lvgl_shares_TaskCb, 100, LV_TASK_PRIO_LOWEST, NULL);

		shares_debug("创建窗口\r\n");
	}
	else
	{
		shares_debug("显示窗口\r\n");
	}

	lv_obj_set_drag_throw(lvgl_shares_data.main_cont, false);
	lv_obj_set_pos(lvgl_shares_data.main_cont, 0, lv_obj_get_height(Fu));
	lvgl_set_obj_show(lvgl_shares_data.main_cont);

	lvgl_shares_anim_Jin();
}


void lvgl_shares_TaskCb(lv_task_t *t)
{
	static int time=0;

	if (lv_obj_get_y(lvgl_shares_data.main_cont) != 0)
	{
		lv_obj_set_drag_throw(lvgl_shares_data.main_cont, true);

	}

	if (lv_obj_get_x(lvgl_shares_data.main_cont) != 0)
	{
		lv_obj_set_drag_throw(lvgl_shares_data.main_cont, false);
	}

}



void lvgl_shares_close(void)
{



	lvgl_desktop_create(lv_scr_act());

	lvgl_task_delete(&lvgl_shares_data.task);

	lvgl_shares_data.point.y = lv_obj_get_y(lvgl_shares_data.main_cont);
	lvgl_shares_data.point.x = lv_obj_get_x(lvgl_shares_data.main_cont);

	lv_obj_clean(lvgl_shares_data.main_cont);

	lv_obj_del(lvgl_shares_data.main_cont);



}


void lvgl_shares_closed(void)
{
	shares_debug("删除窗口\r\n");
	shares_debug("closed\r\n");

	lvgl_shares_data.main_cont = NULL;

}
