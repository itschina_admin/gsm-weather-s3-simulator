#ifndef __LVGL_shares_H__
#define __LVGL_shares_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "lvgl.h"
#include "lvgl_Project.h"
#include "SYSTEM.h"


#if 1
#define shares_debug(format, ...) lvgl_project_debug("[��Ʊ]- ",format,##__VA_ARGS__);
#else
#define shares_debug(format, ...) ;
#endif

#define SHARES_ITEM_HEIGHT 52


typedef enum
{
	SHARES_ZHANGFU_ZHANG=1,
	SHARES_ZHANGFU_JIANG,


}lvgl_shares_zhangfu;

typedef struct
{

	lv_obj_t *main_cont;
	lv_obj_t *lable_name;
	lv_obj_t *lable_code;
	lv_obj_t *lable_zhangfu;
	lv_point_t point;
	char str_name[50];
	char str_code[50];
	char str_zhangfu[50];
	lvgl_shares_zhangfu mode_zhangfu;
	lv_event_cb_t event_cb;

}_lvgl_shares_item_datas;

typedef struct
{

	lv_obj_t *main_cont;
	lv_obj_t *item_cont;

	lv_obj_t *bar_cont;
	lv_obj_t *date_label;
	lv_obj_t *time_label;
	lv_obj_t *item_list;
	lv_obj_t *item_tileview;

	_lvgl_shares_item_datas items[50];


	lv_task_t * task;

	lv_anim_t lv_anim_jin;
	lv_anim_t lv_anim_chu;


	char buf[20];

	int press_index;
	lv_obj_t *press_btn;

	int DongHua_Flag;

	lv_point_t point;

	int item_count;

}_lvgl_shares_data;

extern _lvgl_shares_data lvgl_shares_data;


void lvgl_shares_create(lv_obj_t * Fu);
void lvgl_shares_close(void);
void lvgl_shares_closed(void);
void lvgl_shares_TaskCb(lv_task_t *t);

extern void lvgl_shares_item_create(lv_obj_t * Fu,_lvgl_shares_item_datas *handle);
extern void lvgl_shares_item_add(lv_obj_t * Fu,_lvgl_shares_item_datas *handle);
#ifdef __cplusplus
}
#endif


#endif
