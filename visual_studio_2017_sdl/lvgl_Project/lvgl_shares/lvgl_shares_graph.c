#include "lvgl_shares_graph.h"
#include "lvgl_Project.h"
#include "lvgl_SheZhi.h"
#include "time.h"



_lvgl_shares_graph_data lvgl_shares_graph_data = {
	.main_cont = NULL ,
};


void lvgl_shares_graph_event_cb(struct _lv_obj_t * obj, lv_event_t event)
{
	//if (obj == lvgl_shares_graph_data.main_cont)
	{
		shares_graph_debug("cb:%d\r\n", event);
		switch (event)
		{

		case LV_EVENT_DELETE:

			lvgl_shares_graph_closed();
			break;

		case LV_EVENT_PRESSING:


			break;
		case LV_EVENT_PRESSED:


			break;
		case LV_EVENT_DRAG_END:
			shares_graph_debug("拖动后松手\r\n");
			if (lv_obj_get_y(obj) != 0)
			{

				lv_obj_set_y(obj, 0);
			}

			if (lv_obj_get_x(obj) != 0)
			{
				if (lv_obj_get_x(obj) > 100)
				{
					lvgl_shares_graph_close();
				}
				else
				{
					lv_obj_set_x(obj, 0);
				}
			}
			break;
			break;

		case LV_EVENT_RELEASED:

		case LV_EVENT_LONG_PRESSED:

			break;

		default:
			break;

		}

	}
}

void lvgl_shares_graph_create(lv_obj_t * Fu)
{

	if (lvgl_shares_graph_data.main_cont == NULL)
	{

		//----创建容器----//
		lvgl_shares_graph_data.main_cont = lv_cont_create(Fu, NULL);
		lv_obj_set_pos(lvgl_shares_graph_data.main_cont, 0, 0);
		lv_obj_set_size(lvgl_shares_graph_data.main_cont, lv_obj_get_width(Fu), lv_obj_get_height(Fu));

		//lv_obj_set_click(lvgl_shares_graph_data.main_cont, true); //启用 / 禁用可点击
		lv_obj_set_drag(lvgl_shares_graph_data.main_cont, true);//启用/禁用对象可拖动
		lv_obj_set_drag_dir(lvgl_shares_graph_data.main_cont, LV_DRAG_DIR_HOR);//设置拖动方向
		//lv_obj_set_drag_throw(lvgl_shares_graph_data.main_cont, false);//启用/禁用拖动后是否有惯性移动
		//lv_obj_set_drag_parent(lvgl_shares_graph_data.main_cont, true); //启用 / 禁用父对象可拖动
		lv_obj_add_style(lvgl_shares_graph_data.main_cont, LV_OBJ_PART_MAIN, &lvgl_black_bg_style);//设置样式
		lv_obj_set_event_cb(lvgl_shares_graph_data.main_cont, lvgl_shares_graph_event_cb);//设置回调函数


		//创建一个图表
		lvgl_shares_graph_data.chart = lv_chart_create(lvgl_shares_graph_data.main_cont, NULL);
		lv_obj_set_pos(lvgl_shares_graph_data.chart, 0, 130);
		lv_obj_set_size(lvgl_shares_graph_data.chart, lv_obj_get_width(lvgl_shares_graph_data.main_cont), 130);

		//lv_obj_set_size(chart, 200, 150);
		//lv_obj_align(lvgl_shares_graph_data.chart, NULL, LV_ALIGN_CENTER, 0, 20);
		lv_chart_set_type(lvgl_shares_graph_data.chart, LV_CHART_TYPE_LINE);   /*Show lines and points too*/

		lv_chart_set_div_line_count(lvgl_shares_graph_data.chart, 0, 0);//设置水平垂直分割线条数，默认3，5
		lv_chart_set_range(lvgl_shares_graph_data.chart, 0, 150);//设置y轴数值范围，默认0-100
		lv_chart_set_point_count(lvgl_shares_graph_data.chart, 240);//设置每条数据线所具有的数据点个数,如果不设置的话,则默认值是10
		lv_obj_add_style(lvgl_shares_graph_data.chart, LV_OBJ_PART_MAIN, &lvgl_black_bg_style);//设置样式


		//添加一个渐变色的效果
		lv_obj_set_style_local_bg_opa(lvgl_shares_graph_data.chart, LV_CHART_PART_SERIES, LV_STATE_DEFAULT, LV_OPA_50); /*Max. opa.*/
		lv_obj_set_style_local_bg_grad_dir(lvgl_shares_graph_data.chart, LV_CHART_PART_SERIES, LV_STATE_DEFAULT, LV_GRAD_DIR_VER);
		lv_obj_set_style_local_bg_main_stop(lvgl_shares_graph_data.chart, LV_CHART_PART_SERIES, LV_STATE_DEFAULT, 255);    /*Max opa on the top*/
		lv_obj_set_style_local_bg_grad_stop(lvgl_shares_graph_data.chart, LV_CHART_PART_SERIES, LV_STATE_DEFAULT, 0);      /*Transparent on the bottom*/


		//添加两个数据系列
		lvgl_shares_graph_data.ser1 = lv_chart_add_series(lvgl_shares_graph_data.chart, LV_COLOR_RED);
		lvgl_shares_graph_data.ser2 = lv_chart_add_series(lvgl_shares_graph_data.chart, LV_COLOR_GREEN);
		//lv_chart_init_points(chart, ser2, 200);//给所有数据点设置统一的值
		

		//for (int i = 0; i < 500; i++)
		//{

		//	lv_chart_set_next(chart, ser1, rand() % 150);
		//}

		//for (int i = 0; i < 30; i++)
		//{
		//	lvgl_shares_graph_data.ser2->points[i] = rand()%150;
		//	
		//}


		lv_chart_refresh(lvgl_shares_graph_data.chart); //刷新设置
		lvgl_task_create(&lvgl_shares_graph_data.task,lvgl_shares_graph_TaskCb, 100, LV_TASK_PRIO_LOWEST, NULL);

		shares_graph_debug("创建窗口\r\n");
	}
	else
	{

		shares_graph_debug("显示窗口\r\n");
	}


	lv_obj_set_drag_throw(lvgl_shares_graph_data.main_cont, false);
	//lv_obj_set_pos(lvgl_shares_graph_data.main_cont, 0, lv_obj_get_height(Fu));
	lvgl_set_obj_show(lvgl_shares_graph_data.main_cont);


}


void lvgl_shares_graph_TaskCb(lv_task_t *t)
{
	static int time=0;

	if (lv_obj_get_y(lvgl_shares_graph_data.main_cont) != 0)
	{
		lv_obj_set_drag_throw(lvgl_shares_graph_data.main_cont, true);

	}

	if (lv_obj_get_x(lvgl_shares_graph_data.main_cont) != 0)
	{
		lv_obj_set_drag_throw(lvgl_shares_graph_data.main_cont, false);
	}

	lv_chart_set_next(lvgl_shares_graph_data.chart, lvgl_shares_graph_data.ser1, rand() % 150);


}



void lvgl_shares_graph_close(void)
{



	lvgl_desktop_create(lv_scr_act());

	lvgl_task_delete(&lvgl_shares_graph_data.task);

	lvgl_shares_graph_data.point.y = lv_obj_get_y(lvgl_shares_graph_data.main_cont);
	lvgl_shares_graph_data.point.x = lv_obj_get_x(lvgl_shares_graph_data.main_cont);

	lv_obj_clean(lvgl_shares_graph_data.main_cont);

	lv_obj_del(lvgl_shares_graph_data.main_cont);



}


void lvgl_shares_graph_closed(void)
{
	shares_graph_debug("删除窗口\r\n");
	shares_graph_debug("closed\r\n");

	lvgl_shares_graph_data.main_cont = NULL;

}
