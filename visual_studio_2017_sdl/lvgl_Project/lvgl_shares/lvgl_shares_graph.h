#ifndef __LVGL_shares_graph_H__
#define __LVGL_shares_graph_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "lvgl.h"
#include "lvgl_Project.h"
#include "SYSTEM.h"


#if 1
#define shares_graph_debug(format, ...) lvgl_project_debug("[��Ʊ ͼ��]- ",format,##__VA_ARGS__);
#else
#define shares_graph_debug(format, ...) ;
#endif

#define shares_graph_ITEM_HEIGHT 52


typedef enum
{
	shares_graph_ZHANGFU_ZHANG=1,
	shares_graph_ZHANGFU_JIANG,


}lvgl_shares_graph_zhangfu;

typedef struct
{

	lv_obj_t *main_cont;
	lv_obj_t *lable_name;
	lv_obj_t *lable_code;
	lv_obj_t *lable_zhangfu;
	lv_point_t point;
	char str_name[50];
	char str_code[50];
	char str_zhangfu[50];
	lvgl_shares_graph_zhangfu mode_zhangfu;
	lv_event_cb_t event_cb;



}_lvgl_shares_graph_item_datas;

typedef struct
{

	lv_obj_t *main_cont;


	lv_task_t * task;
	lv_obj_t* chart;
	lv_chart_series_t* ser1;
	lv_chart_series_t* ser2;

	lv_point_t point;
	char buf[20];


}_lvgl_shares_graph_data;

extern _lvgl_shares_graph_data lvgl_shares_graph_data;


void lvgl_shares_graph_create(lv_obj_t * Fu);
void lvgl_shares_graph_close(void);
void lvgl_shares_graph_closed(void);
void lvgl_shares_graph_TaskCb(lv_task_t *t);

#ifdef __cplusplus
}
#endif


#endif
