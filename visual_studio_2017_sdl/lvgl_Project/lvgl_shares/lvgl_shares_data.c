#include "lvgl_shares_data.h"
#include "lvgl_Project.h"
#include "lvgl_SheZhi.h"
#include "stdlib.h"

typedef struct Node
{
	char id[30];
	char name[30];
	int data;
	struct Node* next;
}node;

//创建一个节点
node* ListNodeCreate(void)
{
	node* p = (node*)malloc(sizeof(node));
	return p;
}
//创建和初始化链表
node* List(int* arr, int length)
{
	node* p = (node*)malloc(sizeof(node));
	node* h = NULL;
	node* t = NULL;
	for (int i = 0; i < length; ++i) {
		if (h == NULL) //判断是否为第一块空间
		{	
			h = p;
			t = p;
		}
		else 
		{
			p = (node*)malloc(sizeof(node));//开辟一块新的空间
			t->next = p;//将新空间的地址存入上一个空间的next中
			t = p;//将t所指向的空间修改为新开辟的空间
		}
		p->data = *(arr + i);
		p->next = NULL;//对新开辟的空间赋值
	}
	return h;
}

//输出链表中存储的数据
void ShowList(node* list)
{
	while (list->next) {
		printf("%d ", list->data);
		list = list->next;
	}
	printf("%d\n", list->data);
}

//查找链表中指定位置的数据
int SearchList(node* list, int t)
{
	node* p = list;
	for (int i = 0; i < t; ++i) {
		p = p->next;//将p指向下一个空间
	}
	return p->data;//找到所要求的空间，返回存储的数据
}
int GetListLen(node* list)
{

	node* p = list;
	int i = 1;
	while (p->next != NULL)//找到尾部
	{
		p = p->next;
		i++;
	}
	return i;
}

//在链表中插入数据
node* InsertList(node* list, int data, int t)
{
	node* p = list;
	node* insert = ListNodeCreate();//开辟一块新的空间
	for (int i = 0; i < t - 1; ++i) {
		p = p->next;
	}//找到所插入位置处的上一块空间
	insert->data = data;//存入数据
	insert->next = p->next;//和后面的空间建立连接
	p->next = insert;//和前面的空间建立连接
	return list;
}

//在链表中尾部插入数据
node* InsertListBack(node* list, int data)
{
	node* p = list;
	node* insert = ListNodeCreate();//开辟一块新的空间
	while (p->next != NULL)//找到尾部
	{
		p = p->next;
	}
	insert->data = data;//存入数据
	insert->next = p->next;//和后面的空间建立连接
	p->next = insert;//和前面的空间建立连接
	return list;
}
//在链表中首部插入数据
node* InsertListFront(node* list, int data)
{
	node* p = list;
	node* insert = ListNodeCreate();//开辟一块新的空间
	insert->data = data;
	insert->next = p;
	return insert;
}
//删除指定位置的数据
node* DeleteList(node* list, int t)
{
	node* p = list;
	node* q = NULL;
	if (t) {
		for (int i = 0; p && i < t - 1; ++i) {
			p = p->next;
		}
		if (!p) {
			return list;//若超出链表范围，则直接返回
		}
		q = p;
		p = p->next;
		q->next = p->next;//删除中间结点和尾结点
	}
	else {
		list = p->next;//删除头结点
	}
	free(p);
	return list;
}

//删除指定数据
node* DeleteNumList(node* list, int num)
{
	node* p = list;
	node* q = NULL;
	if (p->data != num) {
		for (; p && (p->data != num); q = p, p = p->next);
		if (!p) {
			return list;//若超出链表范围，则直接返回
		}
		q->next = p->next;//删除中间结点和尾结点
	}
	else {
		list = p->next;//删除头结点
	}
	free(p);
	return list;
}

//修改制定位置的数据
node* ChangeList(node* list, int data, int t)
{
	node* p = list;
	for (int i = 0; i < t; ++i) {
		p = p->next;
	}//找到所要修改的位置
	p->data = data;//修改数据
	return list;
}

//逆序输出链表存储的数据
void ShowListReturn(node* list)
{
	if (list) {
		ShowListReturn(list->next);//递归，访问下一块空间
		printf("%d ", list->data);//输出数据
	}
}


int lvgl_shares_data_test(void)
{
	int arr[] = { 1, 2, 3, 4, 5, 6 ,7,8,9,10};
	node* list = List(arr, sizeof(arr) / sizeof(arr[0]));
	printf("Show:");
	ShowList(list);
	printf("ShowReturn:");
	ShowListReturn(list);
	printf("\n");
	printf("Search:%d\n", SearchList(list, 3));
	printf("Insert:");
	ShowList(list = InsertList(list, 7, 0));
	printf("Insert back:");
	ShowList(list = InsertListBack(list, 100));
	printf("Insert front:");
	ShowList(list = InsertListFront(list, 1000));
	printf("Delete:");
	ShowList(list = DeleteList(list, 2));
	printf("DeleteNum:");
	ShowList(list = DeleteNumList(list, 6));
	printf("Change:");
	ShowList(ChangeList(list, 3, 4));
	printf("List len:%d\r\n", GetListLen(list));

	
	return 0;
}









