#ifndef __LVGL_shares_item_H__
#define __LVGL_shares_item_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "lvgl.h"
#include "lvgl_Project.h"
#include "SYSTEM.h"


#if 1
#define shares_item_debug(format, ...) lvgl_project_debug("[��Ʊ ��Ŀ]- ",format,##__VA_ARGS__);
#else
#define shares_item_debug(format, ...) ;
#endif


typedef struct
{

	lv_obj_t *main_cont;

	lv_obj_t * calendar;

	lv_task_t * task;

	lv_style_t *btn_XuanZhong_style;
	lv_obj_t *btn_XuanZhong;

	lv_anim_t lv_anim_jin;
	lv_anim_t lv_anim_chu;


	char buf[20];

	int press_index;
	lv_obj_t *press_btn;

	int DongHua_Flag;

	lv_point_t point;


}_lvgl_shares_item_data;

extern _lvgl_shares_item_data lvgl_shares_item_data;



void lvgl_shares_item_close(void);
void lvgl_shares_item_closed(void);
void lvgl_shares_item_TaskCb(lv_task_t *t);


#ifdef __cplusplus
}
#endif


#endif
