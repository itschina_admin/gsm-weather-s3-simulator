#include "lvgl_shares_item.h"
#include "lvgl_Project.h"
#include "lvgl_shares.h"

_lvgl_shares_item_data lvgl_shares_item_data;

static void lvgl_shares_item_event_cb(struct _lv_obj_t * obj, lv_event_t event)
{

	uint8_t i = 0;
	for(i=0;i<20;i++)
	{
		if(obj == lvgl_shares_data.items[i].main_cont)
		{
			break;
		}
	}

	//if (obj == lvgl_shares_item_data.main_cont)
	{
		shares_item_debug("cb:%d\r\n", event);
		switch (event)
		{

		case LV_EVENT_DELETE:

			lvgl_shares_data.items[i].main_cont = NULL;
			//lvgl_shares_item_closed();
			break;

		case LV_EVENT_PRESSING:


			break;
		case LV_EVENT_PRESSED:


			break;
		case LV_EVENT_DRAG_END:

			if (lv_obj_get_y(lvgl_shares_item_data.main_cont) != 0)
			{

				lv_obj_set_y(lvgl_shares_item_data.main_cont, 0);
			}

			if (lv_obj_get_x(lvgl_shares_item_data.main_cont) != 0)
			{
				if (lv_obj_get_x(lvgl_shares_item_data.main_cont) > 100)
				{
					lvgl_shares_item_close();
				}
				else
				{
					lv_obj_set_x(lvgl_shares_item_data.main_cont, 0);
				}
			}
			break;
			break;

		case LV_EVENT_RELEASED:

		case LV_EVENT_LONG_PRESSED:

			break;

		default:
			break;

		}
	}
}

void lvgl_shares_item_create(lv_obj_t * Fu,_lvgl_shares_item_datas *handle)
{

	if (handle->main_cont == NULL)
	{

		//----创建容器----//
		handle->main_cont = lv_cont_create(Fu, NULL);
		lv_obj_set_pos(handle->main_cont, handle->point.x, handle->point.y);
		lv_obj_set_size(handle->main_cont, lv_obj_get_width(Fu)-24, SHARES_ITEM_HEIGHT);

		lv_obj_set_drag_parent(handle->main_cont, true); 
		lv_obj_set_click(handle->main_cont, true); 
		lv_obj_set_drag(handle->main_cont, true);
		lv_obj_set_drag_dir(handle->main_cont, LV_DRAG_DIR_VER);
		lv_obj_set_drag_throw(handle->main_cont, false);

		if(handle->mode_zhangfu == SHARES_ZHANGFU_ZHANG)
		{
			lv_obj_add_style(handle->main_cont, LV_OBJ_PART_MAIN, &lvgl_shares_item_red_style);
		}else
		if(handle->mode_zhangfu == SHARES_ZHANGFU_JIANG)
		{
			lv_obj_add_style(handle->main_cont, LV_OBJ_PART_MAIN, &lvgl_shares_item_green_style);
		}

		lv_btn_set_layout(handle->main_cont, LV_LAYOUT_OFF);//关闭对子对象布局

		//名字
		handle->lable_name = lv_label_create(handle->main_cont, NULL);
		lv_label_set_long_mode(handle->lable_name, LV_LABEL_LONG_EXPAND);
		lv_label_set_anim_speed(handle->lable_name, 5);
		lv_label_set_text(handle->lable_name, handle->str_name);
		lv_obj_align(handle->lable_name, NULL, LV_ALIGN_IN_TOP_LEFT, 5, 0);
		lv_obj_add_style(handle->lable_name, LV_OBJ_PART_MAIN, &lvgl_font24_style);

		//代码
		handle->lable_code = lv_label_create(handle->main_cont, NULL);
		lv_label_set_long_mode(handle->lable_code, LV_LABEL_LONG_EXPAND);
		lv_label_set_anim_speed(handle->lable_code, 5);
		lv_label_set_text(handle->lable_code, handle->str_code);
		lv_obj_align(handle->lable_code, NULL, LV_ALIGN_IN_BOTTOM_LEFT, 8, -5);
		lv_obj_add_style(handle->lable_code, LV_OBJ_PART_MAIN, &lvgl_font16_style);

		//涨幅
		handle->lable_zhangfu = lv_label_create(handle->main_cont, NULL);
		lv_label_set_long_mode(handle->lable_zhangfu, LV_LABEL_LONG_EXPAND);
		lv_label_set_anim_speed(handle->lable_zhangfu, 5);
		lv_label_set_text(handle->lable_zhangfu, handle->str_zhangfu);
		lv_obj_align(handle->lable_zhangfu, NULL, LV_ALIGN_IN_BOTTOM_RIGHT, -20, -5);
		lv_obj_add_style(handle->lable_zhangfu, LV_OBJ_PART_MAIN, &lvgl_font16_style);


		lv_obj_set_event_cb(handle->main_cont, handle->event_cb);//设置 btn的事件回调

		//lvgl_task_create(&lvgl_shares_item_data.task,lvgl_shares_item_TaskCb, 100, LV_TASK_PRIO_LOWEST, NULL);

		shares_item_debug("创建窗口\r\n");
	}
	else
	{
		shares_item_debug("显示窗口\r\n");
	}

	//lvgl_shares_item_anim_Jin();
}
void lvgl_shares_item_add(lv_obj_t * Fu,_lvgl_shares_item_datas *handle)
{


}

void lvgl_shares_item_TaskCb(lv_task_t *t)
{
	static int time=0;

	if (lv_obj_get_y(lvgl_shares_item_data.main_cont) != 0)
	{
		lv_obj_set_drag_throw(lvgl_shares_item_data.main_cont, true);

	}

	if (lv_obj_get_x(lvgl_shares_item_data.main_cont) != 0)
	{
		lv_obj_set_drag_throw(lvgl_shares_item_data.main_cont, false);
	}
}

void lvgl_shares_item_close(void)
{

	lvgl_desktop_create(lv_scr_act());

	//lvgl_task_delete(&lvgl_shares_item_data.task);

	lvgl_shares_item_data.point.y = lv_obj_get_y(lvgl_shares_item_data.main_cont);
	lvgl_shares_item_data.point.x = lv_obj_get_x(lvgl_shares_item_data.main_cont);

	lv_obj_clean(lvgl_shares_item_data.main_cont);

	//uint16_t count = lv_obj_count_children(lvgl_shares_item_data.main_cont);
	//printf("count:%d\r\n", count);

	lv_obj_del(lvgl_shares_item_data.main_cont);
}

void lvgl_shares_item_closed(void)
{
	shares_item_debug("删除窗口\r\n");
	shares_item_debug("closed\r\n");

	lvgl_shares_item_data.main_cont = NULL;

}
