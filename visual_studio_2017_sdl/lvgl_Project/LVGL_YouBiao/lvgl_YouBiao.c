#include "lvgl_YouBiao.h"
#include "string.h"
#include "stdio.h"
#include "font.h"

LV_IMG_DECLARE(bmp_ZhiZhen_16x16)
LV_IMG_DECLARE(bmp_ZhiZhen_24x24)


lv_obj_t *lvgl_YouBiao_src=NULL;
lv_obj_t *lvgl_YouBiao_image = NULL;
lv_task_t * lvgl_YouBiao_task = NULL;

void lvgl_YouBiao_SheZhi_GIF(int dat)
{
	if (lvgl_YouBiao_image == NULL)
	{
		lvgl_YouBiao_image = lv_img_create(lv_scr_act(), NULL);
		lv_obj_set_pos(lvgl_YouBiao_image, 0, 0);
	}
	lv_img_set_src(lvgl_YouBiao_image,&bmp_ZhiZhen_16x16);
}


void lvgl_YouBiao_SheZhi_WeiZhi(int x,int y,int sta)
{
	static int x_old=0,y_old=0;

	if(x_old!=x||y_old!=y)
	{
		x_old=x;
		y_old=y;
	}else
	{
		return;
	}

	if (lvgl_YouBiao_image != NULL)
	{
		lv_obj_set_pos(lvgl_YouBiao_image, x, y);

		lv_obj_set_top(lvgl_YouBiao_image,true);
		lv_obj_move_foreground(lvgl_YouBiao_image);
		//lv_obj_invalidate(lvgl_YouBiao_image);
	}	
}

void YouBiao_TaskCb(lv_task_t *t);
void lvgl_YouBiao_create(void)
{

	lvgl_YouBiao_image = lv_img_create(lv_scr_act(), NULL);
	lv_obj_set_pos(lvgl_YouBiao_image, 0, 0);
	lv_img_set_src(lvgl_YouBiao_image, &bmp_ZhiZhen_24x24);

	lvgl_YouBiao_SheZhi_WeiZhi(0,0,0);

	//lvgl_YouBiao_task = lv_task_create(YouBiao_TaskCb, 100, LV_TASK_PRIO_LOWEST, NULL);
}

void YouBiao_TaskCb(lv_task_t *t)
{

}
