#ifndef __lvgl_wenjian_anim_H__
#define __lvgl_wenjian_anim_H__

#ifdef __cplusplus
extern "C" {
#endif


#include "lvgl.h"
#include "lvgl_Project.h"


#if 1
#define lvgl_wenjian_anim_debug(format, ...) lvgl_project_debug("[文件-动效]- ",format,##__VA_ARGS__);
#else
#define lvgl_wenjian_anim_debug(format, ...) ;
#endif


void lvgl_wenjian_anim_biaoti_shangla(void);
void lvgl_wenjian_anim_biaoti_xiala(void);
void lvgl_wenjian_anim_Jin(void);
void lvgl_wenjian_anim_Chu(void);

#ifdef __cplusplus
}
#endif

#endif


