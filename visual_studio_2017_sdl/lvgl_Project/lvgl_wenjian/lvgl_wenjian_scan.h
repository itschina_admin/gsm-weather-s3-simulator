#ifndef _lvgl_wenjian_scan_H__
#define _lvgl_wenjian_scan_H__

#ifdef __cplusplus
extern "C" {
#endif


#include "lvgl_Project.h"




#if 1
#define lvgl_wenjian_scan_debug(format, ...) lvgl_project_debug("[文件-扫描]- ",format,##__VA_ARGS__);
#else
#define lvgl_wenjian_scan_debug(format, ...) ;
#endif


typedef struct _lvgl_wenjian_scan
{
	char *lujing;
	uint16_t shuliang;
	uint8_t shuxing;//0=文件 1=文件夹

	//_lvgl_wenjian_data lvgl_wenjian_data;

}_lvgl_wenjian_scan;


extern _lvgl_wenjian_scan lvgl_wenjian_scan;

#ifdef __cplusplus
}
#endif

#endif

