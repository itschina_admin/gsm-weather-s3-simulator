#include "lvgl_wenjian.h"
#include "lvgl_Project.h"


_lvgl_wenjian_GongNeng_Data lvgl_wenjian_GongNeng_Data;
_lvgl_wenjian_ChuangKou lvgl_wenjian_ChuangKou;

lv_obj_t *lvgl_wenjian_main_cont = NULL;
lv_style_t lvgl_wenjian_style;

const char*wenjian_TEXT[][system_LANGUAGE_NUM] =
{
	{ "文件", "文件", "File", NULL},


};



void lvgl_wenjian_event_cb(struct _lv_obj_t * obj, lv_event_t event)
{
	if (obj == lvgl_wenjian_main_cont)
	{
		//wenjian_debug("cb:%d\r\n", event);
		switch (event)
		{

		case LV_EVENT_DELETE:

			lvgl_wenjian_closed();
			break;

		case LV_EVENT_PRESSING://对象被持续按下


			break;
		case LV_EVENT_PRESSED://对象已被按下


			break;
		case LV_EVENT_DRAG_END://拖动结束后


			if (lv_obj_get_y(lvgl_wenjian_main_cont) != 0)
			{

				lv_obj_set_y(lvgl_wenjian_main_cont, 0);
			}

			if (lv_obj_get_x(lvgl_wenjian_main_cont) != 0)
			{
				if (lv_obj_get_x(lvgl_wenjian_main_cont) > 100)
				{


					lvgl_wenjian_close();


				}
				else
				{
					lv_obj_set_x(lvgl_wenjian_main_cont, 0);
				}
			}
			break;
			break;

		case LV_EVENT_RELEASED://按钮释放

		case LV_EVENT_LONG_PRESSED://按钮长按

			break;

		default:
			break;

		}

	}
}


void lvgl_wenjian_create(lv_obj_t * Fu, char *str, uint32_t color, uint32_t anxia_color)
{

	lvgl_wenjian_GongNeng_Data.color = color;
	lvgl_wenjian_GongNeng_Data.anxia_color = anxia_color;

	if (lvgl_wenjian_main_cont == NULL)
	{

		lv_style_init(&lvgl_wenjian_style);
		lv_style_copy(&lvgl_wenjian_style, &lvgl_WuBianKuang_cont_style);

		//----创建容器----//
		lvgl_wenjian_main_cont = lv_cont_create(lv_scr_act(), NULL);
		lv_obj_set_pos(lvgl_wenjian_main_cont, 0, 0);
		lv_obj_set_size(lvgl_wenjian_main_cont, lv_obj_get_width(Fu), lv_obj_get_height(Fu));
		//lv_obj_set_drag_parent(lvgl_wenjian_main_cont, true); //启用 / 禁用父对象可拖动

		lv_obj_set_click(lvgl_wenjian_main_cont, true); //启用 / 禁用可点击
		lv_obj_set_drag(lvgl_wenjian_main_cont, true);//启用/禁用对象可拖动
		lv_obj_set_drag_dir(lvgl_wenjian_main_cont, LV_DRAG_DIR_ONE);//设置拖动方向
		lv_obj_set_drag_throw(lvgl_wenjian_main_cont, false);//启用/禁用拖动后是否有惯性移动

		//lv_obj_set_drag_parent(lvgl_wenjian_main_cont, true); //启用 / 禁用父对象可拖动
		lv_obj_add_style(lvgl_wenjian_main_cont, LV_OBJ_PART_MAIN, &lvgl_wenjian_style);//设置样式
		lv_obj_set_event_cb(lvgl_wenjian_main_cont, lvgl_wenjian_event_cb);//设置回调函数
		//----创建容器----//



//----标题----//
		lv_style_init(&lvgl_wenjian_ChuangKou.btn_BiaoTi_style);
		lv_style_copy(&lvgl_wenjian_ChuangKou.btn_BiaoTi_style, &lvgl_WuBianKuang_cont_style);

		lv_style_set_radius(&lvgl_wenjian_ChuangKou.btn_BiaoTi_style, LV_STATE_DEFAULT, 10);//圆角

		lv_style_set_bg_opa(&lvgl_wenjian_ChuangKou.btn_BiaoTi_style, LV_STATE_DEFAULT, LV_OPA_COVER);//背景透明度
		lv_style_set_bg_color(&lvgl_wenjian_ChuangKou.btn_BiaoTi_style, LV_STATE_DEFAULT, lv_color_hex(lvgl_wenjian_GongNeng_Data.color));//默认背景上面颜色
		lv_style_set_bg_grad_color(&lvgl_wenjian_ChuangKou.btn_BiaoTi_style, LV_STATE_DEFAULT, lv_color_hex(lvgl_wenjian_GongNeng_Data.color));//默认背景上面颜色

		lv_style_set_bg_color(&lvgl_wenjian_ChuangKou.btn_BiaoTi_style, LV_STATE_PRESSED, lv_color_hex(lvgl_wenjian_GongNeng_Data.anxia_color));//按下时背景颜色
		lv_style_set_bg_grad_color(&lvgl_wenjian_ChuangKou.btn_BiaoTi_style, LV_STATE_PRESSED, lv_color_hex(lvgl_wenjian_GongNeng_Data.anxia_color));//按下时背景颜色

		lv_style_set_transform_height(&lvgl_wenjian_ChuangKou.btn_BiaoTi_style, LV_STATE_PRESSED, -5);
		lv_style_set_transform_width(&lvgl_wenjian_ChuangKou.btn_BiaoTi_style, LV_STATE_PRESSED, -10);
#if LV_USE_ANIMATION
		static lv_anim_path_t path;
		lv_anim_path_init(&path);
		lv_anim_path_set_cb(&path, lv_anim_path_overshoot);

		lv_style_set_transition_prop_1(&lvgl_wenjian_ChuangKou.btn_BiaoTi_style, LV_STATE_DEFAULT, LV_STYLE_TRANSFORM_HEIGHT);
		lv_style_set_transition_prop_2(&lvgl_wenjian_ChuangKou.btn_BiaoTi_style, LV_STATE_DEFAULT, LV_STYLE_TRANSFORM_WIDTH);
		lv_style_set_transition_time(&lvgl_wenjian_ChuangKou.btn_BiaoTi_style, LV_STATE_DEFAULT, 200);
		lv_style_set_transition_path(&lvgl_wenjian_ChuangKou.btn_BiaoTi_style, LV_STATE_DEFAULT, &path);
#endif

		lvgl_wenjian_ChuangKou.btn_BiaoTi = lv_btn_create(lvgl_wenjian_main_cont, NULL);

		lv_obj_set_width(lvgl_wenjian_ChuangKou.btn_BiaoTi, 230);
		lv_obj_set_height(lvgl_wenjian_ChuangKou.btn_BiaoTi, 230);
		lv_obj_set_pos(lvgl_wenjian_ChuangKou.btn_BiaoTi, 5, 5);
		lv_obj_set_event_cb(lvgl_wenjian_ChuangKou.btn_BiaoTi, NULL);//回调函数
		lv_obj_set_drag_parent(lvgl_wenjian_ChuangKou.btn_BiaoTi, true); //启用 / 禁用父对象可拖动
		lv_obj_reset_style_list(lvgl_wenjian_ChuangKou.btn_BiaoTi, LV_BTN_PART_MAIN);//清除原来的样式
		lv_obj_add_style(lvgl_wenjian_ChuangKou.btn_BiaoTi, LV_BTN_PART_MAIN, &lvgl_wenjian_ChuangKou.btn_BiaoTi_style);

		lv_style_init(&lvgl_wenjian_ChuangKou.label_BiaoTi_style);
		lv_style_copy(&lvgl_wenjian_ChuangKou.label_BiaoTi_style, &lvgl_font_ZiDing_K8_24_style);


		lvgl_wenjian_ChuangKou.label_BiaoTi = lv_label_create(lvgl_wenjian_ChuangKou.btn_BiaoTi, NULL);
		lv_label_set_text(lvgl_wenjian_ChuangKou.label_BiaoTi, str);

		lv_label_set_long_mode(lvgl_wenjian_ChuangKou.label_BiaoTi, LV_LABEL_LONG_SROLL_CIRC);//循环滚动
		lv_obj_set_pos(lvgl_wenjian_ChuangKou.label_BiaoTi, 0, 0);
		lv_obj_set_width(lvgl_wenjian_ChuangKou.label_BiaoTi, 240);
		lv_obj_set_height(lvgl_wenjian_ChuangKou.label_BiaoTi, 30);
		lv_label_set_recolor(lvgl_wenjian_ChuangKou.label_BiaoTi, true);
		lv_label_set_align(lvgl_wenjian_ChuangKou.label_BiaoTi, LV_LABEL_ALIGN_CENTER);
		lv_label_set_anim_speed(lvgl_wenjian_ChuangKou.label_BiaoTi, 20);

		lv_obj_align(lvgl_wenjian_ChuangKou.label_BiaoTi, lvgl_wenjian_ChuangKou.btn_BiaoTi, LV_ALIGN_CENTER, 0, 0);//与屏幕居中对齐


		lv_obj_add_style(lvgl_wenjian_ChuangKou.label_BiaoTi, LV_OBJ_PART_MAIN, &lvgl_wenjian_ChuangKou.label_BiaoTi_style);
		//----标题----//

		lvgl_task_create(&lvgl_wenjian_ChuangKou.lvgl_task,lvgl_wenjian_TaskCb, 100, LV_TASK_PRIO_LOWEST, NULL);

		wenjian_debug("创建窗口\r\n");
	}
	else
	{

		wenjian_debug("显示窗口\r\n");
	}



	lv_obj_set_drag_throw(lvgl_wenjian_main_cont, false);//启用/禁用拖动后是否有惯性移动
	lv_obj_set_pos(lvgl_wenjian_main_cont, 0, 0);
	lvgl_set_obj_show(lvgl_wenjian_main_cont);


	lvgl_wenjian_anim_Jin();

}


void lvgl_wenjian_TaskCb(lv_task_t *t)
{
	static int time=0;

	if (lv_obj_get_y(lvgl_wenjian_main_cont) != 0)
	{
		lv_obj_set_drag_throw(lvgl_wenjian_main_cont, true);//启用/禁用拖动后是否有惯性移动

	}

	if (lv_obj_get_x(lvgl_wenjian_main_cont) != 0)
	{
		lv_obj_set_drag_throw(lvgl_wenjian_main_cont, false);//启用/禁用拖动后是否有惯性移动
	}

}



void lvgl_wenjian_close(void)
{
	lvgl_desktop_create(lv_scr_act());

	lvgl_task_delete(&lvgl_wenjian_ChuangKou.lvgl_task);

	lvgl_wenjian_GongNeng_Data.point.y = lv_obj_get_y(lvgl_wenjian_main_cont);
	lvgl_wenjian_GongNeng_Data.point.x = lv_obj_get_x(lvgl_wenjian_main_cont);

	lv_obj_clean(lvgl_wenjian_main_cont);

	uint16_t count = lv_obj_count_children(lvgl_wenjian_main_cont);
	wenjian_debug("count:%d\r\n", count);

	lv_obj_del(lvgl_wenjian_main_cont);
}


void lvgl_wenjian_closed(void)
{
	wenjian_debug("删除窗口\r\n");
	wenjian_debug("closed\r\n");

	lvgl_wenjian_main_cont = NULL;

}
