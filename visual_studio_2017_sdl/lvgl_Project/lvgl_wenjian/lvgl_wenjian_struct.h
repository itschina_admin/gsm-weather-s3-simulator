#ifndef _lvgl_wenjian_struct_H__
#define _lvgl_wenjian_struct_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "lvgl_Project.h"
#include "lvgl_wenjian_struct.h"

#if 1
#define lvgl_wenjian_struct_debug(format, ...) lvgl_project_debug("[文件-结构体]- ",format,##__VA_ARGS__);
#else
#define lvgl_wenjian_struct_debug(format, ...) ;
#endif


typedef struct _lvgl_wenjian_data
{
	char *name;
	char shuxing;
	uint32_t size;

	struct _lvgl_wenjian_data *next;

}_lvgl_wenjian_data;


extern _lvgl_wenjian_data lvgl_wenjian_data;

void lvgl_wenjian_struct_xiaohui(struct _lvgl_wenjian_data *head);
struct _lvgl_wenjian_data * lvgl_wenjian_struct_tianjia(struct _lvgl_wenjian_data *head, int num);
struct _lvgl_wenjian_data * lvgl_wenjian_struct_fanhui(struct _lvgl_wenjian_data *head, int num);
int lvgl_wenjian_struct_zongshuliang(struct _lvgl_wenjian_data *head);

#ifdef __cplusplus
}
#endif

#endif


