#include "lvgl_wenjian_struct.h"
#include "lvgl_Project.h"

_lvgl_wenjian_data lvgl_wenjian_data;

//链表销毁
void lvgl_wenjian_struct_xiaohui(struct _lvgl_wenjian_data *head)
{
	struct _lvgl_wenjian_data *p_sj = NULL, *q_sj = NULL;
	p_sj = head;

	while (p_sj)
	{
		q_sj = p_sj->next;

		sprintf(p_sj->name, "属性销毁");
		memset(p_sj, 0, sizeof(_lvgl_wenjian_data));
		free(p_sj);


		p_sj = q_sj;
	}
	head = NULL;
}


//向链表尾添加num个节点 返回未节点
struct _lvgl_wenjian_data * lvgl_wenjian_struct_tianjia(struct _lvgl_wenjian_data *head, int num)
{

	struct _lvgl_wenjian_data* pNode = head;

	if (pNode == NULL)//无头时
	{
		struct _lvgl_wenjian_data *pNew = (struct _lvgl_wenjian_data*)malloc(sizeof(_lvgl_wenjian_data));
		memset(pNew, 0x00, sizeof(struct _lvgl_wenjian_data));
		pNew->next = NULL;

		pNode = pNew;

	}
	else//有头时
	{
		while (pNode->next != NULL)//找到尾
		{
			pNode = pNode->next;
		}
		for (num; num > 0; num--)
		{
			struct _lvgl_wenjian_data* a = (struct _lvgl_wenjian_data*)malloc(sizeof(struct _lvgl_wenjian_data));
			a->next = NULL;
			memset(a, 0x00, sizeof(_lvgl_wenjian_data));

			pNode->next = a;//向尾部追加节点
			pNode = pNode->next;
		}
	}
	return pNode;
}

//返回链表num节点
struct _lvgl_wenjian_data * lvgl_wenjian_struct_fanhui(struct _lvgl_wenjian_data *head, int num)
{
	struct _lvgl_wenjian_data* pNode = head;

	while (num--)
	{
		pNode = pNode->next;
	}
	return pNode;
}

//返回链表节点数量
int lvgl_wenjian_struct_zongshuliang(struct _lvgl_wenjian_data *head)
{
	int num = 0;

	while (head)
	{
		head = head->next;
		num++;
	}
	return num - 1;
}
