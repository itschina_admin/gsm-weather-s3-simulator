#ifndef __LVGL_wenjian_H__
#define __LVGL_wenjian_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "lvgl.h"
#include "lvgl_Project.h"
#include "SYSTEM.h"


#if 1
#define wenjian_debug(format, ...) lvgl_project_debug("[锟侥硷拷]- ",format,##__VA_ARGS__);
#else
#define wenjian_debug(format, ...) ;
#endif

#define lvgl_wenjian_x 0
#define lvgl_wenjian_y 0
#define lvgl_wenjian_xsize 240
#define lvgl_wenjian_ysize 240

typedef struct
{
	lv_task_t * lvgl_task;


	lv_style_t btn_BiaoTi_style;
	lv_obj_t *btn_BiaoTi;
	lv_obj_t *label_BiaoTi;
	lv_style_t label_BiaoTi_style;
	lv_anim_t lv_anim_biaoti_shangla;
	lv_anim_t lv_anim_biaoti_xiala;

	lv_style_t *btn_XuanZhong_style;
	lv_obj_t *btn_XuanZhong;


	lv_anim_t lv_anim_jin;
	lv_anim_t lv_anim_chu;

}_lvgl_wenjian_ChuangKou;

extern _lvgl_wenjian_ChuangKou lvgl_wenjian_ChuangKou;

typedef struct
{
	char buf[20];

	int AnXia_Num;//锟斤拷锟铰的帮拷钮锟斤拷锟斤拷
	lv_obj_t *AnXia_button;//锟斤拷锟铰的帮拷钮锟斤拷锟斤拷

	int DongHua_Flag;

	lv_point_t point;

	uint32_t color;
	uint32_t anxia_color;

}_lvgl_wenjian_GongNeng_Data;

extern _lvgl_wenjian_GongNeng_Data lvgl_wenjian_GongNeng_Data;

extern lv_obj_t *lvgl_wenjian_main_cont;

void lvgl_wenjian_create(lv_obj_t * Fu, char *str, uint32_t color, uint32_t anxia_color);
void lvgl_wenjian_close(void);
void lvgl_wenjian_closed(void);
void lvgl_wenjian_TaskCb(lv_task_t *t);

#ifdef __cplusplus
}
#endif


#endif
