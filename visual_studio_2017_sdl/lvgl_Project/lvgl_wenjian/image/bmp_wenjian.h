#ifndef __bmp_wenjian_h__
#define __bmp_wenjian_h__


#ifdef __cplusplus
extern "C" {
#endif


#include "lvgl/lvgl.h"


LV_IMG_DECLARE(bmp_wenjian_01)
LV_IMG_DECLARE(bmp_wenjian_02)
LV_IMG_DECLARE(bmp_wenjian_03)
LV_IMG_DECLARE(bmp_wenjian_04)
LV_IMG_DECLARE(bmp_wenjian_05)
LV_IMG_DECLARE(bmp_wenjian_06)


extern const lv_img_dsc_t *bmp_wenjian_buf[6];
#ifdef __cplusplus
}
#endif

#endif

