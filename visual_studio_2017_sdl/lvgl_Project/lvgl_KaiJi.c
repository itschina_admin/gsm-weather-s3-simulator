#include "lvgl_KaiJi.h"
#include "string.h"
#include "stdio.h"
#include "font.h"




LV_IMG_DECLARE(bmp_KaiJi_000)
LV_IMG_DECLARE(bmp_KaiJi_001)
LV_IMG_DECLARE(bmp_KaiJi_002)
LV_IMG_DECLARE(bmp_KaiJi_003)
LV_IMG_DECLARE(bmp_KaiJi_004)
LV_IMG_DECLARE(bmp_KaiJi_005)
LV_IMG_DECLARE(bmp_KaiJi_006)
LV_IMG_DECLARE(bmp_KaiJi_007)
LV_IMG_DECLARE(bmp_KaiJi_008)
LV_IMG_DECLARE(bmp_KaiJi_009)
LV_IMG_DECLARE(bmp_KaiJi_0010)
LV_IMG_DECLARE(bmp_KaiJi_0011)
LV_IMG_DECLARE(bmp_KaiJi_0012)
LV_IMG_DECLARE(bmp_KaiJi_0013)
LV_IMG_DECLARE(bmp_KaiJi_0014)
LV_IMG_DECLARE(bmp_KaiJi_0015)
LV_IMG_DECLARE(bmp_KaiJi_0016)
LV_IMG_DECLARE(bmp_KaiJi_0017)
LV_IMG_DECLARE(bmp_KaiJi_0018)
LV_IMG_DECLARE(bmp_KaiJi_0019)
LV_IMG_DECLARE(bmp_KaiJi_0020)
LV_IMG_DECLARE(bmp_KaiJi_0021)
LV_IMG_DECLARE(bmp_KaiJi_0022)
LV_IMG_DECLARE(bmp_KaiJi_0023)
LV_IMG_DECLARE(bmp_KaiJi_0024)
LV_IMG_DECLARE(bmp_KaiJi_0025)
LV_IMG_DECLARE(bmp_KaiJi_0026)
LV_IMG_DECLARE(bmp_KaiJi_0027)
LV_IMG_DECLARE(bmp_KaiJi_0028)
LV_IMG_DECLARE(bmp_KaiJi_0029)
LV_IMG_DECLARE(bmp_KaiJi_0030)
LV_IMG_DECLARE(bmp_KaiJi_0031)
LV_IMG_DECLARE(bmp_KaiJi_0032)
LV_IMG_DECLARE(bmp_KaiJi_0033)
LV_IMG_DECLARE(bmp_KaiJi_0034)
LV_IMG_DECLARE(bmp_KaiJi_0035)
LV_IMG_DECLARE(bmp_KaiJi_0036)
LV_IMG_DECLARE(bmp_KaiJi_0037)
LV_IMG_DECLARE(bmp_KaiJi_0038)
LV_IMG_DECLARE(bmp_KaiJi_0039)


int gif_num=0;

lv_obj_t *lvgl_KaiJi_src=NULL;
lv_obj_t *lvgl_KaiJi_image = NULL;
lv_task_t * lvgl_KaiJi_task = NULL;
//设置信号值 0-39
void lvgl_SheZhi_GIF(int dat)
{
	const lv_img_dsc_t *bmp_gif[] = 
	{ 
&bmp_KaiJi_000,
&bmp_KaiJi_001,
&bmp_KaiJi_002,
&bmp_KaiJi_003,
&bmp_KaiJi_004,
&bmp_KaiJi_005,
&bmp_KaiJi_006,
&bmp_KaiJi_007,
&bmp_KaiJi_008,
&bmp_KaiJi_009,
&bmp_KaiJi_0010,
&bmp_KaiJi_0011,
&bmp_KaiJi_0012,
&bmp_KaiJi_0013,
&bmp_KaiJi_0014,
&bmp_KaiJi_0015,
&bmp_KaiJi_0016,
&bmp_KaiJi_0017,
&bmp_KaiJi_0018,
&bmp_KaiJi_0019,
&bmp_KaiJi_0020,
&bmp_KaiJi_0021,
&bmp_KaiJi_0022,
&bmp_KaiJi_0023,
&bmp_KaiJi_0024,
&bmp_KaiJi_0025,
&bmp_KaiJi_0026,
&bmp_KaiJi_0027,
&bmp_KaiJi_0028,
&bmp_KaiJi_0029,
&bmp_KaiJi_0030,
&bmp_KaiJi_0031,
&bmp_KaiJi_0032,
&bmp_KaiJi_0033,
&bmp_KaiJi_0034,
&bmp_KaiJi_0035,
&bmp_KaiJi_0036,
&bmp_KaiJi_0037,
&bmp_KaiJi_0038,
&bmp_KaiJi_0039,
	
	};

	if (lvgl_KaiJi_image == NULL)
	{
		lvgl_KaiJi_image = lv_img_create(lvgl_KaiJi_src, NULL);
		lv_obj_set_pos(lvgl_KaiJi_image, 0, 0);
		

	}
	lv_img_set_src(lvgl_KaiJi_image, bmp_gif[dat]);
}
void KaiJi_TaskCb(lv_task_t *t);
void lvgl_KaiJi_create(void)
{

	lvgl_KaiJi_src = lv_obj_create(lv_scr_act(), NULL);


	lv_obj_set_size(lvgl_KaiJi_src, 240, 240);
	lv_obj_clean_style_list(lvgl_KaiJi_src, LV_CONT_PART_MAIN);
	lv_obj_align(lvgl_KaiJi_src, NULL, LV_ALIGN_IN_TOP_LEFT, 0, 0);


	lvgl_SheZhi_GIF(0);

	lvgl_KaiJi_task = lv_task_create(KaiJi_TaskCb, 50, LV_TASK_PRIO_LOWEST, NULL);

}

void KaiJi_TaskCb(lv_task_t *t)
{
	gif_num++;
	//gif_num %= 39;


	if (gif_num > 39)
	{

		if (gif_num > 60)
		{
			lv_task_del(lvgl_KaiJi_task);
			lvgl_KaiJi_task = NULL;
			lv_obj_del(lvgl_KaiJi_src);
		}


	}
	else
	{
		lvgl_SheZhi_GIF(gif_num);
		lv_obj_invalidate(lvgl_KaiJi_src);
		
	}
}
