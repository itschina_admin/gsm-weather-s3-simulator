#ifndef __lvgl_RiLi_anim_H__
#define __lvgl_RiLi_anim_H__

#include "lvgl.h"
#include "lvgl_Project.h"

#ifdef __cplusplus
extern "C" {
#endif



#if 1
#define lvgl_RiLi_anim_debug(format, ...) lvgl_project_debug("[日历-动效]- ",format,##__VA_ARGS__);
#else
#define lvgl_RiLi_anim_debug(format, ...) ;
#endif

void lvgl_RiLi_anim_Jin(void);
void lvgl_RiLi_anim_Chu(void);

#ifdef __cplusplus
}
#endif


#endif


