#include "lvgl_YangShi.h"
#include "string.h"
#include "stdio.h"
#include "font.h"



_lvgl_YangShi_GongNeng_Data lvgl_YangShi_GongNeng_Data;
lv_style_t lvgl_font12_style;
lv_style_t lvgl_font16_style;
lv_style_t lvgl_font24_style;
lv_style_t lvgl_font_acsii_48_style;
lv_style_t lvgl_font_acsii_12_style;
lv_style_t lvgl_font_acsii_32_style;
lv_style_t lvgl_font_ZiDing_K8_24_style;
lv_style_t lvgl_WuBianKuang_cont_style;
lv_style_t lvgl_black_bg_style;
lv_style_t lvgl_blue_bg_style;
lv_style_t lvgl_ta_moren_style;		//�ı�����Ĭ����ʽ
lv_style_t lvgl_touming_cont_style;//͸��cont��ʽ

lv_style_t lvgl_preload_SPINNING_style;		//preload�ײ�Բ��Ĭ����ʽ
lv_style_t lvgl_preload_FILLSPIN_style;		//preload��ת�Ļ���Ĭ����ʽ


lv_style_t lvgl_switch_on_style;			//switch�Ŀ�״̬Ĭ����ʽ
lv_style_t lvgl_switch_background_style;	//switch�ı���Ĭ����ʽ
lv_style_t lvgl_switch_knob_style;			//switch��ָʾ��Ĭ����ʽ


lv_style_t lvgl_shares_item_red_style;
lv_style_t lvgl_shares_item_green_style;

void lvgl_style_create(void)
{


	uint8_t i=0;
	lv_state_t obj = 0;

	lv_state_t status_buf[]={
    LV_STATE_DEFAULT,
    LV_STATE_CHECKED,
    LV_STATE_FOCUSED,
    LV_STATE_EDITED,
    LV_STATE_HOVERED,
    LV_STATE_PRESSED,
    LV_STATE_DISABLED,

	};

	lv_style_init(&lvgl_WuBianKuang_cont_style);
	for(i = 0; i<sizeof(status_buf); i++)
	{
		obj = status_buf[i];

		lv_style_set_radius(&lvgl_WuBianKuang_cont_style, obj, 10);//Բ��
		lv_style_set_border_opa(&lvgl_WuBianKuang_cont_style, obj, LV_OPA_COVER);//�߿�͸����
		lv_style_set_border_width(&lvgl_WuBianKuang_cont_style, obj, 0);//�߿����
		lv_style_set_border_side(&lvgl_WuBianKuang_cont_style, obj, LV_BORDER_SIDE_NONE);
		lv_style_set_border_blend_mode(&lvgl_WuBianKuang_cont_style, obj, LV_BLEND_MODE_NORMAL);
		lv_style_set_pad_top(&lvgl_WuBianKuang_cont_style, obj, 0);//�ڱ߾��ϲ�ߴ�
		lv_style_set_pad_bottom(&lvgl_WuBianKuang_cont_style, obj, 0);//�ڱ߾��²�ߴ�
		lv_style_set_pad_left(&lvgl_WuBianKuang_cont_style, obj, 0);//�ڱ߾����ߴ�
		lv_style_set_pad_right(&lvgl_WuBianKuang_cont_style, obj, 0);//�ڱ߾��Ҳ�ߴ�
		lv_style_set_outline_opa(&lvgl_WuBianKuang_cont_style, obj, LV_OPA_COVER);
		lv_style_set_outline_width(&lvgl_WuBianKuang_cont_style, obj, 0);
		lv_style_set_shadow_width(&lvgl_WuBianKuang_cont_style, obj, 0);


		/*����ɫ*/
		lv_style_set_bg_opa(&lvgl_WuBianKuang_cont_style, obj, LV_OPA_TRANSP);//����͸����
		lv_style_set_bg_color(&lvgl_WuBianKuang_cont_style, obj, LV_COLOR_MAKE(0xff, 0x00, 0x00));//����������ɫ
		lv_style_set_bg_grad_color(&lvgl_WuBianKuang_cont_style, obj, LV_COLOR_MAKE(0xff, 0x00, 0x00));//����������ɫ
		lv_style_set_bg_grad_dir(&lvgl_WuBianKuang_cont_style, obj, LV_GRAD_DIR_VER);//���䷽��

		/*��������ɫλ��*/
		lv_style_set_bg_main_stop(&lvgl_WuBianKuang_cont_style, obj, 0);
		lv_style_set_bg_grad_stop(&lvgl_WuBianKuang_cont_style, obj, 0);
	}


	lv_style_init(&lvgl_black_bg_style);
	for (i = 0; i < sizeof(status_buf); i++)
	{
		obj = status_buf[i];

		lv_style_set_radius(&lvgl_black_bg_style, obj, 0);//Բ��
		lv_style_set_border_opa(&lvgl_black_bg_style, obj, LV_OPA_COVER);//�߿�͸����
		lv_style_set_border_width(&lvgl_black_bg_style, obj, 0);//�߿����
		lv_style_set_border_side(&lvgl_black_bg_style, obj, LV_BORDER_SIDE_NONE);
		lv_style_set_border_blend_mode(&lvgl_black_bg_style, obj, LV_BLEND_MODE_NORMAL);
		lv_style_set_pad_top(&lvgl_black_bg_style, obj, 0);//�ڱ߾��ϲ�ߴ�
		lv_style_set_pad_bottom(&lvgl_black_bg_style, obj, 0);//�ڱ߾��²�ߴ�
		lv_style_set_pad_left(&lvgl_black_bg_style, obj, 0);//�ڱ߾����ߴ�
		lv_style_set_pad_right(&lvgl_black_bg_style, obj, 0);//�ڱ߾��Ҳ�ߴ�
		lv_style_set_outline_opa(&lvgl_black_bg_style, obj, LV_OPA_COVER);
		lv_style_set_outline_width(&lvgl_black_bg_style, obj, 0);
		lv_style_set_shadow_width(&lvgl_black_bg_style, obj, 0);


		/*����ɫ*/
		lv_style_set_bg_opa(&lvgl_black_bg_style, obj, LV_OPA_COVER);//����͸����
		lv_style_set_bg_color(&lvgl_black_bg_style, obj, LV_COLOR_MAKE(0x00, 0x00, 0x00));//����������ɫ
		lv_style_set_bg_grad_color(&lvgl_black_bg_style, obj, LV_COLOR_MAKE(0x00, 0x00, 0x00));//����������ɫ
		lv_style_set_bg_grad_dir(&lvgl_black_bg_style, obj, LV_GRAD_DIR_VER);//���䷽��

		/*��������ɫλ��*/
		lv_style_set_bg_main_stop(&lvgl_black_bg_style, obj, 0);
		lv_style_set_bg_grad_stop(&lvgl_black_bg_style, obj, 0);
	}

	lv_style_init(&lvgl_blue_bg_style);
	for (i = 0; i < sizeof(status_buf); i++)
	{
		obj = status_buf[i];

		lv_style_set_radius(&lvgl_blue_bg_style, obj, 0);//Բ��
		lv_style_set_border_opa(&lvgl_blue_bg_style, obj, LV_OPA_COVER);//�߿�͸����
		lv_style_set_border_width(&lvgl_blue_bg_style, obj, 0);//�߿����
		lv_style_set_border_side(&lvgl_blue_bg_style, obj, LV_BORDER_SIDE_NONE);
		lv_style_set_border_blend_mode(&lvgl_blue_bg_style, obj, LV_BLEND_MODE_NORMAL);
		lv_style_set_pad_top(&lvgl_blue_bg_style, obj, 0);//�ڱ߾��ϲ�ߴ�
		lv_style_set_pad_bottom(&lvgl_blue_bg_style, obj, 0);//�ڱ߾��²�ߴ�
		lv_style_set_pad_left(&lvgl_blue_bg_style, obj, 0);//�ڱ߾����ߴ�
		lv_style_set_pad_right(&lvgl_blue_bg_style, obj, 0);//�ڱ߾��Ҳ�ߴ�
		lv_style_set_outline_opa(&lvgl_blue_bg_style, obj, LV_OPA_COVER);
		lv_style_set_outline_width(&lvgl_blue_bg_style, obj, 0);
		lv_style_set_shadow_width(&lvgl_blue_bg_style, obj, 0);


		/*����ɫ*/
		lv_style_set_bg_opa(&lvgl_blue_bg_style, obj, LV_OPA_COVER);//����͸����
		lv_style_set_bg_color(&lvgl_blue_bg_style, obj, LV_COLOR_MAKE(0x00, 0x00, 0xff));//����������ɫ
		lv_style_set_bg_grad_color(&lvgl_blue_bg_style, obj, LV_COLOR_MAKE(0x00, 0x00, 0xff));//����������ɫ
		lv_style_set_bg_grad_dir(&lvgl_blue_bg_style, obj, LV_GRAD_DIR_VER);//���䷽��

		/*��������ɫλ��*/
		lv_style_set_bg_main_stop(&lvgl_blue_bg_style, obj, 0);
		lv_style_set_bg_grad_stop(&lvgl_blue_bg_style, obj, 0);
	}

	lv_style_init(&lvgl_font_acsii_12_style);
	lv_style_set_text_color(&lvgl_font_acsii_12_style, LV_STATE_DEFAULT, LV_COLOR_MAKE(0xff, 0xff, 0xff));
	lv_style_set_text_font(&lvgl_font_acsii_12_style, LV_STATE_DEFAULT, (const lv_font_t*)&font_acsii_12);

	lv_style_init(&lvgl_font_acsii_32_style);
	lv_style_set_text_color(&lvgl_font_acsii_32_style, LV_STATE_DEFAULT, LV_COLOR_MAKE(0xff, 0xff, 0xff));
	lv_style_set_text_font(&lvgl_font_acsii_32_style, LV_STATE_DEFAULT, (const lv_font_t*)&font_acsii_32);


	lv_style_init(&lvgl_font12_style);
	lv_style_set_text_color(&lvgl_font12_style, LV_STATE_DEFAULT, LV_COLOR_MAKE(0xff, 0xff, 0xff));
	lv_style_set_text_font(&lvgl_font12_style, LV_STATE_DEFAULT, (const lv_font_t*)&font_ChangYongHanZi_12);

	lv_style_init(&lvgl_font16_style);
	lv_style_set_text_color(&lvgl_font16_style, LV_STATE_DEFAULT, LV_COLOR_MAKE(0xff, 0xff, 0xff));
	lv_style_set_text_font(&lvgl_font16_style, LV_STATE_DEFAULT, (const lv_font_t*)&font_ChangYongHanZi_16);


	lv_style_init(&lvgl_font24_style);
	lv_style_set_text_color(&lvgl_font24_style, LV_STATE_DEFAULT, LV_COLOR_MAKE(0xff, 0xff, 0xff));
	lv_style_set_text_font(&lvgl_font24_style, LV_STATE_DEFAULT, (const lv_font_t*)&font_ChangYongHanZi_24);

	lv_style_init(&lvgl_font_acsii_48_style);
	lv_style_set_text_color(&lvgl_font_acsii_48_style, LV_STATE_DEFAULT, LV_COLOR_MAKE(0xff, 0xff, 0xff));
	lv_style_set_text_font(&lvgl_font_acsii_48_style, LV_STATE_DEFAULT, (const lv_font_t*)&font_acsii_48);

	lv_style_init(&lvgl_font_ZiDing_K8_24_style);
	lv_style_set_text_color(&lvgl_font_ZiDing_K8_24_style, LV_STATE_DEFAULT, LV_COLOR_MAKE(0xff, 0xff, 0xff));
	lv_style_set_text_font(&lvgl_font_ZiDing_K8_24_style, LV_STATE_DEFAULT, (const lv_font_t*)&font_ZiDing_K8_24);

//----�����Ĭ����ʽ----//
	lv_style_init(&lvgl_ta_moren_style);
	lv_style_set_text_color(&lvgl_ta_moren_style, LV_STATE_DEFAULT, LV_COLOR_MAKE(0xff, 0xff, 0xff));
	lv_style_set_text_font(&lvgl_ta_moren_style, LV_STATE_DEFAULT, (const lv_font_t*)&font_ZiDing_K8_16);
	lv_style_set_radius(&lvgl_ta_moren_style, LV_STATE_DEFAULT, 6);//Բ��

	lv_style_set_pad_top(&lvgl_ta_moren_style, LV_STATE_DEFAULT, 2);//�ڱ߾��ϲ�ߴ�
	lv_style_set_pad_bottom(&lvgl_ta_moren_style, LV_STATE_DEFAULT, 2);//�ڱ߾��²�ߴ�
	lv_style_set_pad_left(&lvgl_ta_moren_style, LV_STATE_DEFAULT, 2);//�ڱ߾����ߴ�
	lv_style_set_pad_right(&lvgl_ta_moren_style, LV_STATE_DEFAULT, 2);//�ڱ߾��Ҳ�ߴ�

	//Ĭ����ʽ
	lv_style_set_border_opa(&lvgl_ta_moren_style, LV_STATE_DEFAULT, 255);//�߿�͸����
	lv_style_set_border_width(&lvgl_ta_moren_style, LV_STATE_DEFAULT, 1);//�߿����
	lv_style_set_border_color(&lvgl_ta_moren_style, LV_STATE_DEFAULT, LV_COLOR_MAKE(0x00, 0x71, 0xbc));//�߿���ɫ
	lv_style_set_border_side(&lvgl_ta_moren_style, LV_STATE_DEFAULT, LV_BORDER_SIDE_FULL);
	lv_style_set_border_blend_mode(&lvgl_ta_moren_style, LV_STATE_DEFAULT, LV_BLEND_MODE_NORMAL);

	lv_style_set_bg_opa(&lvgl_ta_moren_style, LV_STATE_DEFAULT, LV_OPA_COVER);//����͸����
	lv_style_set_bg_color(&lvgl_ta_moren_style, LV_STATE_DEFAULT, yangshi_moren_bg);//����������ɫ
	lv_style_set_bg_grad_color(&lvgl_ta_moren_style, LV_STATE_DEFAULT, yangshi_moren_bg);//����������ɫ


	//����ʱ
	lv_style_set_border_opa(&lvgl_ta_moren_style, LV_STATE_FOCUSED, 255);//�߿�͸����
	lv_style_set_border_width(&lvgl_ta_moren_style, LV_STATE_FOCUSED, 1);//�߿����
	lv_style_set_border_color(&lvgl_ta_moren_style, LV_STATE_FOCUSED, LV_COLOR_MAKE(0x00, 0x71, 0xbc));//�߿���ɫ
	lv_style_set_border_side(&lvgl_ta_moren_style, LV_STATE_FOCUSED, LV_BORDER_SIDE_FULL);
	lv_style_set_border_blend_mode(&lvgl_ta_moren_style, LV_STATE_FOCUSED, LV_BLEND_MODE_NORMAL);

	lv_style_set_bg_opa(&lvgl_ta_moren_style, LV_STATE_FOCUSED, LV_OPA_COVER);//����͸����
	lv_style_set_bg_color(&lvgl_ta_moren_style, LV_STATE_FOCUSED, yangshi_moren_bg);//����������ɫ
	lv_style_set_bg_grad_color(&lvgl_ta_moren_style, LV_STATE_FOCUSED, yangshi_moren_bg);//����������ɫ

//----�����Ĭ����ʽ----//




	//switch�ı���Ĭ����ʽ
	lv_style_init(&lvgl_switch_background_style);
	lv_style_copy(&lvgl_switch_background_style, &lvgl_WuBianKuang_cont_style);
	lv_style_set_bg_opa(&lvgl_switch_background_style, LV_STATE_DEFAULT, LV_OPA_COVER);//����͸����
	lv_style_set_bg_color(&lvgl_switch_background_style, LV_STATE_DEFAULT, LV_COLOR_MAKE(0xCC, 0xCC, 0xCC));//����������ɫ
	lv_style_set_bg_grad_color(&lvgl_switch_background_style, LV_STATE_DEFAULT, LV_COLOR_MAKE(0xCC, 0xCC, 0xCC));//����������ɫ
	lv_style_set_radius(&lvgl_switch_background_style, LV_STATE_DEFAULT, 15);//Բ��


	lv_style_set_border_opa(&lvgl_switch_background_style, LV_STATE_FOCUSED, LV_OPA_80);//�߿�͸����
	lv_style_set_border_color(&lvgl_switch_background_style, LV_STATE_FOCUSED, LV_COLOR_MAKE(0x00, 0x00, 0xff));//�߿���ɫ
	lv_style_set_border_width(&lvgl_switch_background_style, LV_STATE_FOCUSED, 5);//�߿����


	lv_style_set_shadow_opa(&lvgl_switch_background_style, LV_STATE_FOCUSED, LV_OPA_80);//��Ӱ͸����
	lv_style_set_shadow_color(&lvgl_switch_background_style, LV_STATE_FOCUSED, LV_COLOR_MAKE(0x00, 0x00, 0xff));//��Ӱ��ɫ
	lv_style_set_shadow_width(&lvgl_switch_background_style, LV_STATE_FOCUSED, 5);//��Ӱ����


	//switch�Ŀ�״̬Ĭ����ʽ
	lv_style_init(&lvgl_switch_on_style);
	lv_style_copy(&lvgl_switch_on_style, &lvgl_WuBianKuang_cont_style);
	lv_style_set_bg_opa(&lvgl_switch_on_style, LV_STATE_DEFAULT, LV_OPA_COVER);//����͸����
	lv_style_set_bg_color(&lvgl_switch_on_style, LV_STATE_DEFAULT, LV_COLOR_MAKE(0x41, 0x97, 0xfe));//����������ɫ
	lv_style_set_bg_grad_color(&lvgl_switch_on_style, LV_STATE_DEFAULT, LV_COLOR_MAKE(0x41, 0x97, 0xfe));//����������ɫ
	lv_style_set_radius(&lvgl_switch_on_style, LV_STATE_DEFAULT, 15);//Բ��


	//switch��ָʾ��Ĭ����ʽ
	lv_style_init(&lvgl_switch_knob_style);
	lv_style_copy(&lvgl_switch_knob_style, &lvgl_WuBianKuang_cont_style);
	lv_style_set_bg_opa(&lvgl_switch_knob_style, LV_STATE_DEFAULT, LV_OPA_COVER);//����͸����
	lv_style_set_bg_color(&lvgl_switch_knob_style, LV_STATE_DEFAULT, LV_COLOR_MAKE(0xff, 0xff, 0xff));//����������ɫ
	lv_style_set_bg_grad_color(&lvgl_switch_knob_style, LV_STATE_DEFAULT, LV_COLOR_MAKE(0xff, 0xff, 0xff));//����������ɫ
	lv_style_set_radius(&lvgl_switch_knob_style, LV_STATE_DEFAULT, 15);//Բ��
	lv_style_set_pad_top(&lvgl_switch_knob_style, LV_STATE_DEFAULT, -3);//�ڱ߾��ϲ�ߴ�
	lv_style_set_pad_bottom(&lvgl_switch_knob_style, LV_STATE_DEFAULT, -3);//�ڱ߾��²�ߴ�
	lv_style_set_pad_left(&lvgl_switch_knob_style, LV_STATE_DEFAULT, -3);//�ڱ߾����ߴ�
	lv_style_set_pad_right(&lvgl_switch_knob_style, LV_STATE_DEFAULT, -3);//�ڱ߾��Ҳ�ߴ�

	lv_style_set_shadow_opa(&lvgl_switch_knob_style, LV_STATE_DEFAULT, LV_OPA_80);//��Ӱ͸����
	lv_style_set_shadow_color(&lvgl_switch_knob_style, LV_STATE_DEFAULT, LV_COLOR_MAKE(0x00, 0x00, 0x00));//��Ӱ��ɫ
	lv_style_set_shadow_width(&lvgl_switch_knob_style, LV_STATE_DEFAULT, 5);//��Ӱ����




	//----͸������������ʽ----//
	lv_style_init(&lvgl_touming_cont_style);
	lv_style_set_radius(&lvgl_touming_cont_style, LV_STATE_DEFAULT, 0);
	lv_style_set_border_opa(&lvgl_touming_cont_style, LV_STATE_DEFAULT, 255);//�߿�͸����
	lv_style_set_border_width(&lvgl_touming_cont_style, LV_STATE_DEFAULT, 0);//�߿����
	lv_style_set_border_side(&lvgl_touming_cont_style, LV_STATE_DEFAULT, LV_BORDER_SIDE_NONE);
	lv_style_set_border_blend_mode(&lvgl_touming_cont_style, LV_STATE_DEFAULT, LV_BLEND_MODE_NORMAL);
	lv_style_set_pad_top(&lvgl_touming_cont_style, LV_STATE_DEFAULT, 0);//�ڱ߾��ϲ�ߴ�
	lv_style_set_pad_bottom(&lvgl_touming_cont_style, LV_STATE_DEFAULT, 0);//�ڱ߾��²�ߴ�
	lv_style_set_pad_left(&lvgl_touming_cont_style, LV_STATE_DEFAULT, 0);//�ڱ߾����ߴ�
	lv_style_set_pad_right(&lvgl_touming_cont_style, LV_STATE_DEFAULT, 0);//�ڱ߾��Ҳ�ߴ�
	//����
	lv_style_set_bg_opa(&lvgl_touming_cont_style, LV_STATE_DEFAULT, LV_OPA_0);//����͸����
	lv_style_set_bg_color(&lvgl_touming_cont_style, LV_STATE_DEFAULT, LV_COLOR_MAKE(0x00, 0x5B, 0x97));//����������ɫ
	lv_style_set_bg_grad_color(&lvgl_touming_cont_style, LV_STATE_DEFAULT, LV_COLOR_MAKE(0x93, 0x93, 0x93));//����������ɫ

	//----͸������������ʽ----//


	YangShi_RiLi();
	lvgl_kb_yangshi_init();
	lvgl_yangshi_preload();

	lvgl_share_item_style_init();
}


lv_style_t lvgl_rili_bg_style;				//��������Ĭ����ʽ
lv_style_t lvgl_rili_header_style;			//����headerĬ����ʽ
lv_style_t lvgl_rili_day_name_style;		//����day_nameĬ����ʽ
lv_style_t lvgl_rili_date_style;			//����dateĬ����ʽ




void YangShi_RiLi(void)
{

	lv_style_init(&lvgl_rili_bg_style);

	lv_style_set_radius(&lvgl_rili_bg_style, LV_STATE_DEFAULT , 0);//Բ��

	lv_style_set_border_opa(&lvgl_rili_bg_style, LV_STATE_DEFAULT , LV_OPA_COVER);//�߿�͸����
	lv_style_set_border_width(&lvgl_rili_bg_style, LV_STATE_DEFAULT , 2);//�߿����
	lv_style_set_border_side(&lvgl_rili_bg_style, LV_STATE_DEFAULT , LV_BORDER_SIDE_FULL);
	lv_style_set_border_blend_mode(&lvgl_rili_bg_style, LV_STATE_DEFAULT , LV_BLEND_MODE_NORMAL);
	lv_style_set_border_color(&lvgl_rili_bg_style, LV_STATE_DEFAULT, rili_biankuang_bg_color);//�߿���ɫ



	lv_style_set_pad_top(&lvgl_rili_bg_style, LV_STATE_DEFAULT , 2);//�ڱ߾��ϲ�ߴ�
	lv_style_set_pad_bottom(&lvgl_rili_bg_style, LV_STATE_DEFAULT , 2);//�ڱ߾��²�ߴ�
	lv_style_set_pad_left(&lvgl_rili_bg_style, LV_STATE_DEFAULT , 2);//�ڱ߾����ߴ�
	lv_style_set_pad_right(&lvgl_rili_bg_style, LV_STATE_DEFAULT , 2);//�ڱ߾��Ҳ�ߴ�



	lv_style_set_shadow_opa(&lvgl_rili_bg_style, LV_STATE_DEFAULT, LV_OPA_COVER);//��Ӱ͸����
	lv_style_set_shadow_color(&lvgl_rili_bg_style, LV_STATE_DEFAULT, LV_COLOR_MAKE(0x00, 0x00, 0x00));//��Ӱ��ɫ
	lv_style_set_shadow_width(&lvgl_rili_bg_style, LV_STATE_DEFAULT, 0);//��Ӱ����

	lv_style_set_bg_opa(&lvgl_rili_bg_style, LV_STATE_DEFAULT , LV_OPA_COVER);//����͸����
	lv_style_set_bg_color(&lvgl_rili_bg_style, LV_STATE_DEFAULT , rili_bg_color);//����������ɫ
	lv_style_set_bg_grad_color(&lvgl_rili_bg_style, LV_STATE_DEFAULT , rili_bg_color);//����������ɫ

//-----�۽�ʱ-----//
	lv_style_set_border_color(&lvgl_rili_bg_style, LV_STATE_FOCUSED, rili_biankuang_bg_color);//�߿���ɫ


	lv_style_set_bg_opa(&lvgl_rili_bg_style, LV_STATE_FOCUSED, LV_OPA_COVER);//����͸����
	lv_style_set_bg_color(&lvgl_rili_bg_style, LV_STATE_FOCUSED, rili_bg_color);//����������ɫ
	lv_style_set_bg_grad_color(&lvgl_rili_bg_style, LV_STATE_FOCUSED, rili_bg_color);//����������ɫ


	lv_style_set_shadow_opa(&lvgl_rili_bg_style, LV_STATE_FOCUSED, LV_OPA_COVER);//��Ӱ͸����
	lv_style_set_shadow_color(&lvgl_rili_bg_style, LV_STATE_FOCUSED, LV_COLOR_MAKE(0x00, 0x00, 0x00));//��Ӱ��ɫ
	lv_style_set_shadow_width(&lvgl_rili_bg_style, LV_STATE_FOCUSED, 0);//��Ӱ����
//-----�۽�ʱ-----//


	lv_style_set_text_color(&lvgl_rili_bg_style, LV_STATE_DEFAULT, LV_COLOR_MAKE(0x00, 0x00, 0x00));//������ɫ

	lv_style_set_text_color(&lvgl_rili_bg_style, LV_STATE_FOCUSED, LV_COLOR_MAKE(0x00, 0x00, 0x00));//������ɫ




//----����headerĬ����ʽ----//
	lv_style_init(&lvgl_rili_header_style);
	lv_style_copy(&lvgl_rili_header_style, &lvgl_rili_bg_style);


	lv_style_set_bg_opa(&lvgl_rili_header_style, LV_STATE_DEFAULT, LV_OPA_COVER);//����͸����
	lv_style_set_bg_color(&lvgl_rili_header_style, LV_STATE_DEFAULT, rili_biankuang_bg_color);//����������ɫ
	lv_style_set_bg_grad_color(&lvgl_rili_header_style, LV_STATE_DEFAULT, rili_biankuang_bg_color);//����������ɫ



	lv_style_set_text_color(&lvgl_rili_header_style, LV_STATE_DEFAULT, LV_COLOR_MAKE(0xFF, 0xFF, 0xFF));//������ɫ
	lv_style_set_text_font(&lvgl_rili_header_style, LV_STATE_DEFAULT, (const lv_font_t*)&lv_font_montserrat_24);//����

	//----�۽�ʱ----//
	lv_style_set_bg_opa(&lvgl_rili_header_style, LV_STATE_FOCUSED, LV_OPA_COVER);//����͸����
	lv_style_set_bg_color(&lvgl_rili_header_style, LV_STATE_FOCUSED, rili_biankuang_bg_color);//����������ɫ
	lv_style_set_bg_grad_color(&lvgl_rili_header_style, LV_STATE_FOCUSED, rili_biankuang_bg_color);//����������ɫ




	lv_style_set_text_color(&lvgl_rili_header_style, LV_STATE_FOCUSED, LV_COLOR_MAKE(0xFF, 0xFF, 0xFF));//������ɫ
	lv_style_set_text_font(&lvgl_rili_header_style, LV_STATE_FOCUSED, (const lv_font_t*)&lv_font_montserrat_24);//����
	//----�۽�ʱ----//



//----����day_nameĬ����ʽ----// ���� ���ڴ���������ɫ
	lv_style_init(&lvgl_rili_day_name_style);
	lv_style_copy(&lvgl_rili_day_name_style, &lvgl_rili_bg_style);

	lv_style_set_bg_opa(&lvgl_rili_day_name_style, LV_STATE_DEFAULT, LV_OPA_COVER);//����͸����
	lv_style_set_bg_color(&lvgl_rili_day_name_style, LV_STATE_DEFAULT, rili_biankuang_bg_color);//����������ɫ
	lv_style_set_bg_grad_color(&lvgl_rili_day_name_style, LV_STATE_DEFAULT, rili_biankuang_bg_color);//����������ɫ



	lv_style_set_text_color(&lvgl_rili_day_name_style, LV_STATE_DEFAULT, LV_COLOR_MAKE(0x00, 0x7f, 0xff));//������ɫ
	lv_style_set_text_font(&lvgl_rili_day_name_style, LV_STATE_DEFAULT, (const lv_font_t*)&font_ZiDing_K8_12);//����

	//----�۽�ʱ----//
	lv_style_set_bg_opa(&lvgl_rili_day_name_style, LV_STATE_FOCUSED, LV_OPA_COVER);//����͸����
	lv_style_set_bg_color(&lvgl_rili_day_name_style, LV_STATE_FOCUSED, rili_biankuang_bg_color);//����������ɫ
	lv_style_set_bg_grad_color(&lvgl_rili_day_name_style, LV_STATE_FOCUSED, rili_biankuang_bg_color);//����������ɫ


	lv_style_set_text_color(&lvgl_rili_day_name_style, LV_STATE_FOCUSED, LV_COLOR_MAKE(0x00, 0x7f, 0xff));//������ɫ
	lv_style_set_text_font(&lvgl_rili_day_name_style, LV_STATE_FOCUSED, (const lv_font_t*)&font_ZiDing_K8_12);//����
	//----�۽�ʱ----//




//----����dateĬ����ʽ----//
	lv_style_init(&lvgl_rili_date_style);
	//lv_style_copy(&lvgl_rili_date_style, &lvgl_rili_bg_style);


	lv_style_set_radius(&lvgl_rili_date_style, LV_STATE_DEFAULT, 20);//Բ��

	lv_style_set_border_opa(&lvgl_rili_date_style, LV_STATE_DEFAULT, LV_OPA_COVER);//�߿�͸����
	lv_style_set_border_width(&lvgl_rili_date_style, LV_STATE_DEFAULT, 0);//�߿����
	lv_style_set_border_side(&lvgl_rili_date_style, LV_STATE_DEFAULT, LV_BORDER_SIDE_NONE);
	lv_style_set_border_blend_mode(&lvgl_rili_date_style, LV_STATE_DEFAULT, LV_BLEND_MODE_NORMAL);
	lv_style_set_border_color(&lvgl_rili_date_style, LV_STATE_DEFAULT, rili_biankuang_bg_color);//�߿���ɫ



	lv_style_set_pad_top(&lvgl_rili_date_style, LV_STATE_DEFAULT, 2);//�ڱ߾��ϲ�ߴ�
	lv_style_set_pad_bottom(&lvgl_rili_date_style, LV_STATE_DEFAULT, 2);//�ڱ߾��²�ߴ�
	lv_style_set_pad_left(&lvgl_rili_date_style, LV_STATE_DEFAULT, 2);//�ڱ߾����ߴ�
	lv_style_set_pad_right(&lvgl_rili_date_style, LV_STATE_DEFAULT, 2);//�ڱ߾��Ҳ�ߴ�


	lv_style_set_bg_opa(&lvgl_rili_date_style, LV_STATE_DEFAULT, LV_OPA_COVER);//����͸����
	lv_style_set_bg_color(&lvgl_rili_date_style, LV_STATE_DEFAULT, rili_bg_color);//����������ɫ
	lv_style_set_bg_grad_color(&lvgl_rili_date_style, LV_STATE_DEFAULT, rili_bg_color);//����������ɫ


	lv_style_set_text_color(&lvgl_rili_date_style, LV_STATE_DEFAULT, LV_COLOR_MAKE(0xff, 0xff, 0xff));//������ɫ
	lv_style_set_text_font(&lvgl_rili_date_style, LV_STATE_DEFAULT, (const lv_font_t*)&lv_font_montserrat_16);//����

	//----�۽�ʱ----//
	lv_style_set_bg_opa(&lvgl_rili_date_style, LV_STATE_FOCUSED, LV_OPA_COVER);//����͸����
	lv_style_set_bg_color(&lvgl_rili_date_style, LV_STATE_FOCUSED, LV_COLOR_MAKE(0x55, 0x96, 0xd8));//����������ɫ
	lv_style_set_bg_grad_color(&lvgl_rili_date_style, LV_STATE_FOCUSED, LV_COLOR_MAKE(0x55, 0x96, 0xd8));//����������ɫ


	lv_style_set_text_color(&lvgl_rili_date_style, LV_STATE_FOCUSED, LV_COLOR_MAKE(0xff, 0xff, 0xff));//������ɫ
	lv_style_set_text_font(&lvgl_rili_date_style, LV_STATE_FOCUSED, (const lv_font_t*)&lv_font_montserrat_16);//����
	//----�۽�ʱ----//


	//----����ʱ----//
	lv_style_set_bg_opa(&lvgl_rili_date_style, LV_STATE_PRESSED, LV_OPA_COVER);//����͸����
	lv_style_set_bg_color(&lvgl_rili_date_style, LV_STATE_PRESSED, LV_COLOR_MAKE(0x55, 0x55, 0x55));//����������ɫ
	lv_style_set_bg_grad_color(&lvgl_rili_date_style, LV_STATE_PRESSED, LV_COLOR_MAKE(0x55, 0x55, 0x55));//����������ɫ


	lv_style_set_text_color(&lvgl_rili_date_style, LV_STATE_PRESSED, LV_COLOR_MAKE(0xff, 0xff, 0xff));//������ɫ
	lv_style_set_text_font(&lvgl_rili_date_style, LV_STATE_PRESSED, (const lv_font_t*)&lv_font_montserrat_16);//����
	//----����ʱ----//





}

lv_style_t lvgl_kb_TEXT_LOWER_moren_style;		//���̱���Ĭ����ʽ
lv_style_t lvgl_kb_TEXT_UPPER_moren_style;		//���̰�ťĬ����ʽ

void lvgl_kb_yangshi_init(void)
{
	//---���̰�ťĬ����ʽ----//
	lv_style_init(&lvgl_kb_TEXT_LOWER_moren_style);
	lv_style_set_text_color(&lvgl_kb_TEXT_LOWER_moren_style, LV_STATE_DEFAULT, LV_COLOR_MAKE(0xff, 0xff, 0xff));
	//lv_style_set_text_font(&lvgl_kb_TEXT_LOWER_moren_style, LV_STATE_DEFAULT, (const lv_font_t*)&font_ZiDing_K8_16);
	lv_style_set_radius(&lvgl_kb_TEXT_LOWER_moren_style, LV_STATE_DEFAULT, 6);//Բ��

	lv_style_set_pad_top(&lvgl_kb_TEXT_LOWER_moren_style, LV_STATE_DEFAULT, 0);//�ڱ߾��ϲ�ߴ�
	lv_style_set_pad_bottom(&lvgl_kb_TEXT_LOWER_moren_style, LV_STATE_DEFAULT, 4);//�ڱ߾��²�ߴ�
	lv_style_set_pad_left(&lvgl_kb_TEXT_LOWER_moren_style, LV_STATE_DEFAULT, 0);//�ڱ߾����ߴ�
	lv_style_set_pad_right(&lvgl_kb_TEXT_LOWER_moren_style, LV_STATE_DEFAULT, 0);//�ڱ߾��Ҳ�ߴ�

	//Ĭ����ʽ
	lv_style_set_border_opa(&lvgl_kb_TEXT_LOWER_moren_style, LV_STATE_DEFAULT, 255);//�߿�͸����
	lv_style_set_border_width(&lvgl_kb_TEXT_LOWER_moren_style, LV_STATE_DEFAULT, 0);//�߿����
	lv_style_set_border_color(&lvgl_kb_TEXT_LOWER_moren_style, LV_STATE_DEFAULT, LV_COLOR_MAKE(0x00, 0x71, 0xbc));//�߿���ɫ
	lv_style_set_border_side(&lvgl_kb_TEXT_LOWER_moren_style, LV_STATE_DEFAULT, LV_BORDER_SIDE_NONE);
	lv_style_set_border_blend_mode(&lvgl_kb_TEXT_LOWER_moren_style, LV_STATE_DEFAULT, LV_BLEND_MODE_NORMAL);

	lv_style_set_bg_opa(&lvgl_kb_TEXT_LOWER_moren_style, LV_STATE_DEFAULT, LV_OPA_COVER);//����͸����
	lv_style_set_bg_color(&lvgl_kb_TEXT_LOWER_moren_style, LV_STATE_DEFAULT, yangshi_moren_bg);//����������ɫ
	lv_style_set_bg_grad_color(&lvgl_kb_TEXT_LOWER_moren_style, LV_STATE_DEFAULT, yangshi_moren_bg);//����������ɫ


	//����ʱ
	lv_style_set_border_opa(&lvgl_kb_TEXT_LOWER_moren_style, LV_STATE_FOCUSED, 255);//�߿�͸����
	lv_style_set_border_width(&lvgl_kb_TEXT_LOWER_moren_style, LV_STATE_FOCUSED, 0);//�߿����
	lv_style_set_border_color(&lvgl_kb_TEXT_LOWER_moren_style, LV_STATE_FOCUSED, LV_COLOR_MAKE(0x00, 0x71, 0xbc));//�߿���ɫ
	lv_style_set_border_side(&lvgl_kb_TEXT_LOWER_moren_style, LV_STATE_FOCUSED, LV_BORDER_SIDE_NONE);
	lv_style_set_border_blend_mode(&lvgl_kb_TEXT_LOWER_moren_style, LV_STATE_FOCUSED, LV_BLEND_MODE_NORMAL);

	lv_style_set_bg_opa(&lvgl_kb_TEXT_LOWER_moren_style, LV_STATE_FOCUSED, LV_OPA_COVER);//����͸����
	lv_style_set_bg_color(&lvgl_kb_TEXT_LOWER_moren_style, LV_STATE_FOCUSED, yangshi_moren_bg);//����������ɫ
	lv_style_set_bg_grad_color(&lvgl_kb_TEXT_LOWER_moren_style, LV_STATE_FOCUSED, yangshi_moren_bg);//����������ɫ

//----���̰�ťĬ����ʽ----//


//---���̰�ťĬ����ʽ----//
	lv_style_init(&lvgl_kb_TEXT_UPPER_moren_style);
	lv_style_set_text_color(&lvgl_kb_TEXT_UPPER_moren_style, LV_STATE_DEFAULT, LV_COLOR_MAKE(0xff, 0xff, 0xff));
	//lv_style_set_text_font(&lvgl_kb_TEXT_UPPER_moren_style, LV_STATE_DEFAULT, (const lv_font_t*)&font_ZiDing_K8_16);
	lv_style_set_radius(&lvgl_kb_TEXT_UPPER_moren_style, LV_STATE_DEFAULT, 6);//Բ��

	lv_style_set_pad_top(&lvgl_kb_TEXT_UPPER_moren_style, LV_STATE_DEFAULT, 0);//�ڱ߾��ϲ�ߴ�
	lv_style_set_pad_bottom(&lvgl_kb_TEXT_UPPER_moren_style, LV_STATE_DEFAULT, 0);//�ڱ߾��²�ߴ�
	lv_style_set_pad_left(&lvgl_kb_TEXT_UPPER_moren_style, LV_STATE_DEFAULT, 0);//�ڱ߾����ߴ�
	lv_style_set_pad_right(&lvgl_kb_TEXT_UPPER_moren_style, LV_STATE_DEFAULT, 0);//�ڱ߾��Ҳ�ߴ�


	//Ĭ����ʽ
	lv_style_set_border_opa(&lvgl_kb_TEXT_UPPER_moren_style, LV_STATE_DEFAULT, 255);//�߿�͸����
	lv_style_set_border_width(&lvgl_kb_TEXT_UPPER_moren_style, LV_STATE_DEFAULT, 1);//�߿����
	lv_style_set_border_color(&lvgl_kb_TEXT_UPPER_moren_style, LV_STATE_DEFAULT, LV_COLOR_MAKE(0x00, 0x71, 0xbc));//�߿���ɫ
	lv_style_set_border_side(&lvgl_kb_TEXT_UPPER_moren_style, LV_STATE_DEFAULT, LV_BORDER_SIDE_FULL);
	lv_style_set_border_blend_mode(&lvgl_kb_TEXT_UPPER_moren_style, LV_STATE_DEFAULT, LV_BLEND_MODE_NORMAL);

	lv_style_set_bg_opa(&lvgl_kb_TEXT_UPPER_moren_style, LV_STATE_DEFAULT, LV_OPA_COVER);//����͸����
	lv_style_set_bg_color(&lvgl_kb_TEXT_UPPER_moren_style, LV_STATE_DEFAULT, yangshi_moren_bg);//����������ɫ
	lv_style_set_bg_grad_color(&lvgl_kb_TEXT_UPPER_moren_style, LV_STATE_DEFAULT, yangshi_moren_bg);//����������ɫ


	//����ʱ
	lv_style_set_border_opa(&lvgl_kb_TEXT_UPPER_moren_style, LV_STATE_FOCUSED, 255);//�߿�͸����
	lv_style_set_border_width(&lvgl_kb_TEXT_UPPER_moren_style, LV_STATE_FOCUSED, 1);//�߿����
	lv_style_set_border_color(&lvgl_kb_TEXT_UPPER_moren_style, LV_STATE_FOCUSED, LV_COLOR_MAKE(0x00, 0x71, 0xbc));//�߿���ɫ
	lv_style_set_border_side(&lvgl_kb_TEXT_UPPER_moren_style, LV_STATE_FOCUSED, LV_BORDER_SIDE_FULL);
	lv_style_set_border_blend_mode(&lvgl_kb_TEXT_UPPER_moren_style, LV_STATE_FOCUSED, LV_BLEND_MODE_NORMAL);

	lv_style_set_bg_opa(&lvgl_kb_TEXT_UPPER_moren_style, LV_STATE_FOCUSED, LV_OPA_COVER);//����͸����
	lv_style_set_bg_color(&lvgl_kb_TEXT_UPPER_moren_style, LV_STATE_FOCUSED, yangshi_moren_bg);//����������ɫ
	lv_style_set_bg_grad_color(&lvgl_kb_TEXT_UPPER_moren_style, LV_STATE_FOCUSED, yangshi_moren_bg);//����������ɫ

//----���̰�ťĬ����ʽ----//


}

lv_style_t lvgl_preload_SPINNING_style;		//preload�ײ�Բ��Ĭ����ʽ
lv_style_t lvgl_preload_FILLSPIN_style;		//preload��ת�Ļ���Ĭ����ʽ

void lvgl_yangshi_preload(void)
{

	//----preload�ײ�԰Ĭ����ʽ----//
	lv_style_init(&lvgl_preload_SPINNING_style);
	lv_style_set_text_color(&lvgl_preload_SPINNING_style, LV_STATE_DEFAULT, LV_COLOR_MAKE(0xff, 0xff, 0xff));
	lv_style_set_text_font(&lvgl_preload_SPINNING_style, LV_STATE_DEFAULT, (const lv_font_t*)&font_ZiDing_K8_16);
	lv_style_set_radius(&lvgl_preload_SPINNING_style, LV_STATE_DEFAULT, 6);//Բ��

	lv_style_set_pad_top(&lvgl_preload_SPINNING_style, LV_STATE_DEFAULT, 0);//�ڱ߾��ϲ�ߴ�
	lv_style_set_pad_bottom(&lvgl_preload_SPINNING_style, LV_STATE_DEFAULT, 0);//�ڱ߾��²�ߴ�
	lv_style_set_pad_left(&lvgl_preload_SPINNING_style, LV_STATE_DEFAULT, 0);//�ڱ߾����ߴ�
	lv_style_set_pad_right(&lvgl_preload_SPINNING_style, LV_STATE_DEFAULT, 0);//�ڱ߾��Ҳ�ߴ�

	//������ʽ
	lv_style_set_line_color(&lvgl_preload_SPINNING_style, LV_STATE_DEFAULT, LV_COLOR_MAKE(0xff, 0xff, 0xff));
	lv_style_set_line_width(&lvgl_preload_SPINNING_style, LV_STATE_DEFAULT, 10);

	//Ĭ����ʽ
	lv_style_set_border_opa(&lvgl_preload_SPINNING_style, LV_STATE_DEFAULT, LV_OPA_TRANSP);//�߿�͸����
	lv_style_set_border_width(&lvgl_preload_SPINNING_style, LV_STATE_DEFAULT, 1);//�߿����
	lv_style_set_border_color(&lvgl_preload_SPINNING_style, LV_STATE_DEFAULT, LV_COLOR_MAKE(0x00, 0x71, 0xbc));//�߿���ɫ
	lv_style_set_border_side(&lvgl_preload_SPINNING_style, LV_STATE_DEFAULT, LV_BORDER_SIDE_NONE);
	lv_style_set_border_blend_mode(&lvgl_preload_SPINNING_style, LV_STATE_DEFAULT, LV_BLEND_MODE_NORMAL);

	lv_style_set_bg_opa(&lvgl_preload_SPINNING_style, LV_STATE_DEFAULT, LV_OPA_TRANSP);//����͸����
	lv_style_set_bg_color(&lvgl_preload_SPINNING_style, LV_STATE_DEFAULT, yangshi_moren_bg);//����������ɫ
	lv_style_set_bg_grad_color(&lvgl_preload_SPINNING_style, LV_STATE_DEFAULT, yangshi_moren_bg);//����������ɫ

//----preload�ײ�԰Ĭ����ʽ----//

	//----preload��ת�Ļ���Ĭ����ʽ----//
	lv_style_init(&lvgl_preload_FILLSPIN_style);
	lv_style_set_text_color(&lvgl_preload_FILLSPIN_style, LV_STATE_DEFAULT, LV_COLOR_MAKE(0xff, 0xff, 0xff));
	lv_style_set_text_font(&lvgl_preload_FILLSPIN_style, LV_STATE_DEFAULT, (const lv_font_t*)&font_ZiDing_K8_16);
	lv_style_set_radius(&lvgl_preload_FILLSPIN_style, LV_STATE_DEFAULT, 6);//Բ��

	lv_style_set_pad_top(&lvgl_preload_FILLSPIN_style, LV_STATE_DEFAULT, 2);//�ڱ߾��ϲ�ߴ�
	lv_style_set_pad_bottom(&lvgl_preload_FILLSPIN_style, LV_STATE_DEFAULT, 2);//�ڱ߾��²�ߴ�
	lv_style_set_pad_left(&lvgl_preload_FILLSPIN_style, LV_STATE_DEFAULT, 2);//�ڱ߾����ߴ�
	lv_style_set_pad_right(&lvgl_preload_FILLSPIN_style, LV_STATE_DEFAULT, 2);//�ڱ߾��Ҳ�ߴ�

	//������ʽ
	lv_style_set_line_color(&lvgl_preload_FILLSPIN_style, LV_STATE_DEFAULT, LV_COLOR_MAKE(0xff, 0xff, 0xff));
	lv_style_set_line_width(&lvgl_preload_FILLSPIN_style, LV_STATE_DEFAULT, 7);

	//Ĭ����ʽ
	lv_style_set_border_opa(&lvgl_preload_FILLSPIN_style, LV_STATE_DEFAULT, LV_OPA_TRANSP);//�߿�͸����
	lv_style_set_border_width(&lvgl_preload_FILLSPIN_style, LV_STATE_DEFAULT, 1);//�߿����
	lv_style_set_border_color(&lvgl_preload_FILLSPIN_style, LV_STATE_DEFAULT, LV_COLOR_MAKE(0x00, 0x71, 0xbc));//�߿���ɫ
	lv_style_set_border_side(&lvgl_preload_FILLSPIN_style, LV_STATE_DEFAULT, LV_BORDER_SIDE_NONE);
	lv_style_set_border_blend_mode(&lvgl_preload_FILLSPIN_style, LV_STATE_DEFAULT, LV_BLEND_MODE_NORMAL);

	lv_style_set_bg_opa(&lvgl_preload_FILLSPIN_style, LV_STATE_DEFAULT, LV_OPA_TRANSP);//����͸����
	lv_style_set_bg_color(&lvgl_preload_FILLSPIN_style, LV_STATE_DEFAULT, yangshi_moren_bg);//����������ɫ
	lv_style_set_bg_grad_color(&lvgl_preload_FILLSPIN_style, LV_STATE_DEFAULT, yangshi_moren_bg);//����������ɫ

	//----preload��ת�Ļ���Ĭ����ʽ----//
}

void lvgl_share_item_style_init(void)
{
	lv_state_t obj = 0;

	obj=LV_STATE_DEFAULT;

	lv_style_init(&lvgl_shares_item_red_style);
	lv_style_set_radius(&lvgl_shares_item_red_style, obj, 10);//Բ��
	lv_style_set_border_opa(&lvgl_shares_item_red_style, obj, LV_OPA_COVER);//�߿�͸����
	lv_style_set_border_width(&lvgl_shares_item_red_style, obj, 0);//�߿����
	lv_style_set_border_side(&lvgl_shares_item_red_style, obj, LV_BORDER_SIDE_NONE);
	lv_style_set_border_blend_mode(&lvgl_shares_item_red_style, obj, LV_BLEND_MODE_NORMAL);
	lv_style_set_pad_top(&lvgl_shares_item_red_style, obj, 0);//�ڱ߾��ϲ�ߴ�
	lv_style_set_pad_bottom(&lvgl_shares_item_red_style, obj, 0);//�ڱ߾��²�ߴ�
	lv_style_set_pad_left(&lvgl_shares_item_red_style, obj, 0);//�ڱ߾����ߴ�
	lv_style_set_pad_right(&lvgl_shares_item_red_style, obj, 0);//�ڱ߾��Ҳ�ߴ�
	lv_style_set_outline_opa(&lvgl_shares_item_red_style, obj, LV_OPA_COVER);
	lv_style_set_outline_width(&lvgl_shares_item_red_style, obj, 0);
	lv_style_set_shadow_width(&lvgl_shares_item_red_style, obj, 0);



	/*����ɫ*/
	lv_style_set_bg_opa(&lvgl_shares_item_red_style, obj, LV_OPA_20);//����͸����
	lv_style_set_bg_color(&lvgl_shares_item_red_style, obj, LV_COLOR_MAKE(0xff, 0x00, 0x00));//����������ɫ
	lv_style_set_bg_grad_color(&lvgl_shares_item_red_style, obj, LV_COLOR_MAKE(0xff, 0x00, 0x00));//����������ɫ
	lv_style_set_bg_grad_dir(&lvgl_shares_item_red_style, obj, LV_GRAD_DIR_VER);//���䷽��

	/*��������ɫλ��*/
	lv_style_set_bg_main_stop(&lvgl_shares_item_red_style, obj, 0);
	lv_style_set_bg_main_stop(&lvgl_shares_item_red_style, obj, 0);


	obj=LV_STATE_PRESSED;

	lv_style_set_radius(&lvgl_shares_item_red_style, obj, 10);//Բ��
	lv_style_set_border_opa(&lvgl_shares_item_red_style, obj, LV_OPA_COVER);//�߿�͸����
	lv_style_set_border_width(&lvgl_shares_item_red_style, obj, 0);//�߿����
	lv_style_set_border_side(&lvgl_shares_item_red_style, obj, LV_BORDER_SIDE_NONE);
	lv_style_set_border_blend_mode(&lvgl_shares_item_red_style, obj, LV_BLEND_MODE_NORMAL);
	lv_style_set_pad_top(&lvgl_shares_item_red_style, obj, 0);//�ڱ߾��ϲ�ߴ�
	lv_style_set_pad_bottom(&lvgl_shares_item_red_style, obj, 0);//�ڱ߾��²�ߴ�
	lv_style_set_pad_left(&lvgl_shares_item_red_style, obj, 0);//�ڱ߾����ߴ�
	lv_style_set_pad_right(&lvgl_shares_item_red_style, obj, 0);//�ڱ߾��Ҳ�ߴ�
	lv_style_set_outline_opa(&lvgl_shares_item_red_style, obj, LV_OPA_COVER);
	lv_style_set_outline_width(&lvgl_shares_item_red_style, obj, 0);
	lv_style_set_shadow_width(&lvgl_shares_item_red_style, obj, 0);

	/*����ɫ*/
	lv_style_set_bg_opa(&lvgl_shares_item_red_style, obj, LV_OPA_20);//����͸����
	lv_style_set_bg_color(&lvgl_shares_item_red_style, obj, LV_COLOR_MAKE(0xff, 0x00, 0x00));//����������ɫ
	lv_style_set_bg_grad_color(&lvgl_shares_item_red_style, obj, LV_COLOR_MAKE(0xff, 0x00, 0x00));//����������ɫ
	lv_style_set_bg_grad_dir(&lvgl_shares_item_red_style, obj, LV_GRAD_DIR_VER);//���䷽��
	/*��������ɫλ��*/
	lv_style_set_bg_main_stop(&lvgl_shares_item_red_style, obj, 0);
	lv_style_set_bg_main_stop(&lvgl_shares_item_red_style, obj, 0);

	obj=LV_STATE_FOCUSED;

	lv_style_set_radius(&lvgl_shares_item_red_style, obj, 10);//Բ��
	lv_style_set_border_opa(&lvgl_shares_item_red_style, obj, LV_OPA_COVER);//�߿�͸����
	lv_style_set_border_width(&lvgl_shares_item_red_style, obj, 0);//�߿����
	lv_style_set_border_side(&lvgl_shares_item_red_style, obj, LV_BORDER_SIDE_NONE);
	lv_style_set_border_blend_mode(&lvgl_shares_item_red_style, obj, LV_BLEND_MODE_NORMAL);
	lv_style_set_pad_top(&lvgl_shares_item_red_style, obj, 0);//�ڱ߾��ϲ�ߴ�
	lv_style_set_pad_bottom(&lvgl_shares_item_red_style, obj, 0);//�ڱ߾��²�ߴ�
	lv_style_set_pad_left(&lvgl_shares_item_red_style, obj, 0);//�ڱ߾����ߴ�
	lv_style_set_pad_right(&lvgl_shares_item_red_style, obj, 0);//�ڱ߾��Ҳ�ߴ�

	lv_style_set_outline_opa(&lvgl_shares_item_red_style, obj, LV_OPA_COVER);
	lv_style_set_outline_width(&lvgl_shares_item_red_style, obj, 0);

	lv_style_set_shadow_width(&lvgl_shares_item_red_style, obj, 0);

	/*����ɫ*/
	lv_style_set_bg_opa(&lvgl_shares_item_red_style, obj, LV_OPA_20);//����͸����
	lv_style_set_bg_color(&lvgl_shares_item_red_style, obj, LV_COLOR_MAKE(0xff, 0x00, 0x00));//����������ɫ
	lv_style_set_bg_grad_color(&lvgl_shares_item_red_style, obj, LV_COLOR_MAKE(0xff, 0x00, 0x00));//����������ɫ
	lv_style_set_bg_grad_dir(&lvgl_shares_item_red_style, obj, LV_GRAD_DIR_VER);//���䷽��
	/*��������ɫλ��*/
	lv_style_set_bg_main_stop(&lvgl_shares_item_red_style, obj, 0);
	lv_style_set_bg_main_stop(&lvgl_shares_item_red_style, obj, 0);




#if LV_USE_ANIMATION
	//Add a transition to the size change
	static lv_anim_path_t path_red;
	lv_anim_path_init(&path_red);
	lv_anim_path_set_cb(&path_red, lv_anim_path_overshoot);
	lv_style_set_transform_height(&lvgl_shares_item_red_style, LV_STATE_PRESSED, -5);
	lv_style_set_transform_width(&lvgl_shares_item_red_style, LV_STATE_PRESSED, -10);
	lv_style_set_transition_prop_1(&lvgl_shares_item_red_style, LV_STATE_DEFAULT, LV_STYLE_TRANSFORM_HEIGHT);
	lv_style_set_transition_prop_2(&lvgl_shares_item_red_style, LV_STATE_DEFAULT, LV_STYLE_TRANSFORM_WIDTH);
	lv_style_set_transition_time(&lvgl_shares_item_red_style, LV_STATE_DEFAULT, 200);
	lv_style_set_transition_path(&lvgl_shares_item_red_style, LV_STATE_DEFAULT, &path_red);
#endif



	obj=LV_STATE_DEFAULT;

	lv_style_init(&lvgl_shares_item_green_style);
	lv_style_set_radius(&lvgl_shares_item_green_style, obj, 10);//Բ��
	lv_style_set_border_opa(&lvgl_shares_item_green_style, obj, LV_OPA_COVER);//�߿�͸����
	lv_style_set_border_width(&lvgl_shares_item_green_style, obj, 0);//�߿����
	lv_style_set_border_side(&lvgl_shares_item_green_style, obj, LV_BORDER_SIDE_NONE);
	lv_style_set_border_blend_mode(&lvgl_shares_item_green_style, obj, LV_BLEND_MODE_NORMAL);
	lv_style_set_pad_top(&lvgl_shares_item_green_style, obj, 0);//�ڱ߾��ϲ�ߴ�
	lv_style_set_pad_bottom(&lvgl_shares_item_green_style, obj, 0);//�ڱ߾��²�ߴ�
	lv_style_set_pad_left(&lvgl_shares_item_green_style, obj, 0);//�ڱ߾����ߴ�
	lv_style_set_pad_right(&lvgl_shares_item_green_style, obj, 0);//�ڱ߾��Ҳ�ߴ�
	lv_style_set_outline_opa(&lvgl_shares_item_green_style, obj, LV_OPA_COVER);
	lv_style_set_outline_width(&lvgl_shares_item_green_style, obj, 0);
	lv_style_set_shadow_width(&lvgl_shares_item_green_style, obj, 0);



	/*����ɫ*/
	lv_style_set_bg_opa(&lvgl_shares_item_green_style, obj, LV_OPA_20);//����͸����
	lv_style_set_bg_color(&lvgl_shares_item_green_style, obj, LV_COLOR_MAKE(0x00, 0xff, 0x00));//����������ɫ
	lv_style_set_bg_grad_color(&lvgl_shares_item_green_style, obj, LV_COLOR_MAKE(0x00, 0xff, 0x00));//����������ɫ
	lv_style_set_bg_grad_dir(&lvgl_shares_item_green_style, obj, LV_GRAD_DIR_VER);//���䷽��

	/*��������ɫλ��*/
	lv_style_set_bg_main_stop(&lvgl_shares_item_green_style, obj, 0);
	lv_style_set_bg_main_stop(&lvgl_shares_item_green_style, obj, 0);


	obj=LV_STATE_PRESSED;

	lv_style_set_radius(&lvgl_shares_item_green_style, obj, 10);//Բ��
	lv_style_set_border_opa(&lvgl_shares_item_green_style, obj, LV_OPA_COVER);//�߿�͸����
	lv_style_set_border_width(&lvgl_shares_item_green_style, obj, 0);//�߿����
	lv_style_set_border_side(&lvgl_shares_item_green_style, obj, LV_BORDER_SIDE_NONE);
	lv_style_set_border_blend_mode(&lvgl_shares_item_green_style, obj, LV_BLEND_MODE_NORMAL);
	lv_style_set_pad_top(&lvgl_shares_item_green_style, obj, 0);//�ڱ߾��ϲ�ߴ�
	lv_style_set_pad_bottom(&lvgl_shares_item_green_style, obj, 0);//�ڱ߾��²�ߴ�
	lv_style_set_pad_left(&lvgl_shares_item_green_style, obj, 0);//�ڱ߾����ߴ�
	lv_style_set_pad_right(&lvgl_shares_item_green_style, obj, 0);//�ڱ߾��Ҳ�ߴ�
	lv_style_set_outline_opa(&lvgl_shares_item_green_style, obj, LV_OPA_COVER);
	lv_style_set_outline_width(&lvgl_shares_item_green_style, obj, 0);
	lv_style_set_shadow_width(&lvgl_shares_item_green_style, obj, 0);

	/*����ɫ*/
	lv_style_set_bg_opa(&lvgl_shares_item_green_style, obj, LV_OPA_20);//����͸����
	lv_style_set_bg_color(&lvgl_shares_item_green_style, obj, LV_COLOR_MAKE(0x00, 0xff, 0x00));//����������ɫ
	lv_style_set_bg_grad_color(&lvgl_shares_item_green_style, obj, LV_COLOR_MAKE(0x00, 0xff, 0x00));//����������ɫ
	lv_style_set_bg_grad_dir(&lvgl_shares_item_green_style, obj, LV_GRAD_DIR_VER);//���䷽��
	/*��������ɫλ��*/
	lv_style_set_bg_main_stop(&lvgl_shares_item_green_style, obj, 0);
	lv_style_set_bg_main_stop(&lvgl_shares_item_green_style, obj, 0);

	obj=LV_STATE_FOCUSED;

	lv_style_set_radius(&lvgl_shares_item_green_style, obj, 10);//Բ��
	lv_style_set_border_opa(&lvgl_shares_item_green_style, obj, LV_OPA_COVER);//�߿�͸����
	lv_style_set_border_width(&lvgl_shares_item_green_style, obj, 0);//�߿����
	lv_style_set_border_side(&lvgl_shares_item_green_style, obj, LV_BORDER_SIDE_NONE);
	lv_style_set_border_blend_mode(&lvgl_shares_item_green_style, obj, LV_BLEND_MODE_NORMAL);
	lv_style_set_pad_top(&lvgl_shares_item_green_style, obj, 0);//�ڱ߾��ϲ�ߴ�
	lv_style_set_pad_bottom(&lvgl_shares_item_green_style, obj, 0);//�ڱ߾��²�ߴ�
	lv_style_set_pad_left(&lvgl_shares_item_green_style, obj, 0);//�ڱ߾����ߴ�
	lv_style_set_pad_right(&lvgl_shares_item_green_style, obj, 0);//�ڱ߾��Ҳ�ߴ�

	lv_style_set_outline_opa(&lvgl_shares_item_green_style, obj, LV_OPA_COVER);
	lv_style_set_outline_width(&lvgl_shares_item_green_style, obj, 0);

	lv_style_set_shadow_width(&lvgl_shares_item_green_style, obj, 0);

	/*����ɫ*/
	lv_style_set_bg_opa(&lvgl_shares_item_green_style, obj, LV_OPA_20);//����͸����
	lv_style_set_bg_color(&lvgl_shares_item_green_style, obj, LV_COLOR_MAKE(0x00, 0xff, 0x00));//����������ɫ
	lv_style_set_bg_grad_color(&lvgl_shares_item_green_style, obj, LV_COLOR_MAKE(0x00, 0xff, 0x00));//����������ɫ
	lv_style_set_bg_grad_dir(&lvgl_shares_item_green_style, obj, LV_GRAD_DIR_VER);//���䷽��
	/*��������ɫλ��*/
	lv_style_set_bg_main_stop(&lvgl_shares_item_green_style, obj, 0);
	lv_style_set_bg_main_stop(&lvgl_shares_item_green_style, obj, 0);




#if LV_USE_ANIMATION
	//Add a transition to the size change
	static lv_anim_path_t path_green;
	lv_anim_path_init(&path_green);
	lv_anim_path_set_cb(&path_green, lv_anim_path_overshoot);
	lv_style_set_transform_height(&lvgl_shares_item_green_style, LV_STATE_PRESSED, -5);
	lv_style_set_transform_width(&lvgl_shares_item_green_style, LV_STATE_PRESSED, -10);
	lv_style_set_transition_prop_1(&lvgl_shares_item_green_style, LV_STATE_DEFAULT, LV_STYLE_TRANSFORM_HEIGHT);
	lv_style_set_transition_prop_2(&lvgl_shares_item_green_style, LV_STATE_DEFAULT, LV_STYLE_TRANSFORM_WIDTH);
	lv_style_set_transition_time(&lvgl_shares_item_green_style, LV_STATE_DEFAULT, 200);
	lv_style_set_transition_path(&lvgl_shares_item_green_style, LV_STATE_DEFAULT, &path_green);
#endif
}
