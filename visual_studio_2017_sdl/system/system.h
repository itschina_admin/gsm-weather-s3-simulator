#ifndef __SYS_TEM_H__
#define __SYS_TEM_H__


#ifdef __cplusplus
extern "C" {
#endif

#include "stdio.h"
#include "lvgl_Project.h"

#define WIFI_SCAN_LIST_SIZE 10

// "daily": [{                          //返回指定days天数的结果
//   "date": "2015-09-20",              //日期
//   "text_day": "多云",                //白天天气现象文字
//   "code_day": "4",                  //白天天气现象代码
//   "text_night": "晴",               //晚间天气现象文字
//   "code_night": "0",                //晚间天气现象代码
//   "high": "26",                     //当天最高温度
//   "low": "17",                      //当天最低温度
//   "precip": "0",                    //降水概率，范围0~100，单位百分比（目前仅支持国外城市）
//   "wind_direction": "",             //风向文字
//   "wind_direction_degree": "255",   //风向角度，范围0~360
//   "wind_speed": "9.66",             //风速，单位km/h（当unit=c时）、mph（当unit=f时）
//   "wind_scale": "",                 //风力等级
//   "rainfall": "0.0",                //降水量，单位mm
//   "humidity": "76"                  //相对湿度，0~100，单位为百分比


#if lvgl_win

typedef struct {
	uint8_t bssid[6];                     /**< MAC address of AP */
	uint8_t ssid[33];                     /**< SSID of AP */
	uint8_t primary;                      /**< channel of AP */
	int8_t  rssi;                         /**< signal strength of AP */
	uint32_t phy_11b : 1;                   /**< bit: 0 flag to identify if 11b mode is enabled or not */
	uint32_t phy_11g : 1;                   /**< bit: 1 flag to identify if 11g mode is enabled or not */
	uint32_t phy_11n : 1;                   /**< bit: 2 flag to identify if 11n mode is enabled or not */
	uint32_t phy_lr : 1;                    /**< bit: 3 flag to identify if low rate is enabled or not */
	uint32_t wps : 1;                       /**< bit: 4 flag to identify if WPS is supported or not */
	uint32_t reserved : 27;                 /**< bit: 5..31 reserved */
} wifi_ap_record_t;

#endif

typedef struct
{
    char date[20];             		    //日期
    char text_day[20];                  //白天天气现象文字
    char code_day[20];                 //白天天气现象代码
    char text_night[20];                //晚间天气现象文字
    char code_night[20];                //晚间天气现象代码
    char high[20];                     //当天最高温度
    char low[20];                      //当天最低温度
    char precip[20];                    //降水概率，范围0~100，单位百分比（目前仅支持国外城市）
    char wind_direction[20];            //风向文字
    char wind_direction_degree[20];   //风向角度，范围0~360
    char wind_speed[20];             //风速，单位km/h（当unit=c时）、mph（当unit=f时）
    char wind_scale[20];                 //风力等级
    char rainfall[20];                //降水量，单位mm
    char humidity[20];                  //相对湿度，0~100，单位为百分比

}_Weather_Data;


typedef struct
{
    char aqi[10]; //空气质量指数(AQI)是描述空气质量状况的定量指数
    char pm25[10]; //PM2.5颗粒物（粒径小于等于2.5μm）1小时平均值。单位：μg/m³
    char pm10[10]; //PM10颗粒物（粒径小于等于10μm）1小时平均值。单位：μg/m³
    char so2[10]; //二氧化硫1小时平均值。单位：μg/m³
    char no2[10]; //二氧化氮1小时平均值。单位：μg/m³
    char co[10]; //一氧化碳1小时平均值。单位：mg/m³
    char o3[10]; //臭氧1小时平均值。单位：μg/m³
    char primary_pollutant[10]; //首要污染物
    char quality[10]; //空气质量类别，有“优、良、轻度污染、中度污染、重度污染、严重污染”6类
	char last_update[30]; //数据发布时间
}_Air_Data;

typedef struct
{
	 char date[20];         //日期
    char sunrise[10];      //日出时间
    char sunset[10];       //日落时间


}_RiLuo_Data;


typedef struct
{
  char riqi[20];
  char shijian[20];
	int nian;
  int yue;
  int ri;
  int shi;
  int fen;
  int miao;
  int xingqi;

}_RiQi_Data;

typedef struct
{
	int sd_sta;
	int sht20_sta;
	int ft5206_sta;
	int spiffs_sta;
	int nvs_sta;
	int wm8978_sta;
	int mpu6050_sta;

	uint8_t MAC[6];
	char ip[30];
	char ziwangyanma[30];
	char wangguan[30];
	char wifi_name[32];
	char wifi_password[64];


	char IP_ADDR[30];
	char City[30];

	//wifi链接成功事件
	//EventGroupHandle_t wifi_event_group;
	//EventGroupHandle_t Wav_event_group;
	//EventGroupHandle_t TianQi_event_group;

	//xSemaphoreHandle  https_request_Semaphore;


	_Weather_Data   Weather_Data[3];
	//_kongqi_Data    kongqi_Data;
	_RiLuo_Data     RiLuo_Data[3];
	_RiQi_Data      RiQi_Data;
	//_laohuangli_data laohuangli_data;

	//_bilibili_Data bilibili_Data;

	//SHT2x_data SHT20;

	int Temp;
	int Humi;
	int HuoQu_TianQi_Flag;
	int HuoQu_ShiJian_Flag;
	int HuoQu_LaoHuangLi_Flag;
	int HuoQu_bilibili_Flag;
	int WIFI_Sta;//WIFI连接状态
	int web_huoqu_tianqi_flag;
	int wifi_scan_flag;

	int Language;
	int wifi_scan_list_cnt;
	wifi_ap_record_t wifi_ap_info[WIFI_SCAN_LIST_SIZE];	// AP信息结构体大小

	int wifi_kaiguan;

}_system_data;

extern _system_data system_data;

#define system_LANGUAGE_NUM 4

enum Language_Num {
	JianTiZhongWen = 0,
	FanTiZhongWen,
	YingWen,
	QiTa,
};

enum wifi_sta {
	wifi_weilianjie = 0,
	wifi_yilianjie,
	wifi_lianjiehzong,
	wifi_lianjieshibai,
	wifi_qingqiuduankai,
	wifi_yiduankai,
};

//extern wavctrl WaveCtrlData;
//extern FILE* Wave_File;


int system_init(void);
int system_set_Language(int dat);
int system_get_Language(void);

int system_set_wifi_kaiguan(int dat);
int system_get_wifi_kaiguan(void);


#ifdef __cplusplus
}
#endif
#endif
